package djf.ui;

import djf.AppTemplate;
import djf.language.LanguageException;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import properties_manager.PropertiesManager;

import static djf.AppPropertyType.*;

public class SettingsController {
    private AppTemplate app;

    public SettingsController(AppTemplate initApp) {
        app = initApp;
    }

    public void processLanguageRequest() {
        try {
            app.getLanguageSettings().promptForLanguage();
            app.getLanguageSettings().resetLanguage();
        } catch (LanguageException le) {
            System.out.println("Error Loading Laguage into UI");
        }
    }

    public void processHelpRequest() {
        // GET THE PATH OF THE HTML DOCUMENT THAT
        // CONTAINS THE APPLICATION HELP AND DISPLAY IT

    }

    public void processAboutRequest() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        String title = PropertiesManager.getPropertiesManager().getProperty(ABOUT_TITLE);
        String nameAndCompany = PropertiesManager.getPropertiesManager().getProperty(ABOUT_CONTENT);
        String devlopers = PropertiesManager.getPropertiesManager().getProperty(ABOUT_DEVELOPERS);
        String framworks = PropertiesManager.getPropertiesManager().getProperty(ABOUT_FRAMEWORKS);
        alert.setHeaderText("About Metro Map Maker");
        alert.setTitle(title);
        alert.setContentText(nameAndCompany +"\n" + devlopers + "\n" + framworks);
        alert.showAndWait();
    }
}
