package djf.ui;

import static djf.AppPropertyType.*;

import djf.AppTemplate;

import static djf.language.AppLanguageSettings.PATH_EXPORT;
import static djf.language.AppLanguageSettings.PATH_WORK;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import djf.components.AppWelcomeComponent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;

public class FileController {
    // HERE'S THE APP
    AppTemplate app;

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    boolean saved;

    // THIS IS THE FILE FOR THE WORK CURRENTLY BEING WORKED ON
    File currentWorkFile;

    /**
     * This constructor just keeps the app for later.
     *
     * @param initApp The application within which this controller will provide file toolbar responses.
     */
    public FileController(AppTemplate initApp) {
        // NOTHING YET
        saved = true;
        app = initApp;
    }

    /**
     * This method starts the process of editing new Work. If work is already being edited, it will prompt the user to
     * save it first.
     */
    public void processNewRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                String mapName = "";
                Boolean check = true;
                while (check == true) {
                    TextInputDialog textInputDialog = new TextInputDialog();
                    textInputDialog.setContentText("Enter the name for the map");

                    Optional<String> result = textInputDialog.showAndWait();
                    if (result.isPresent()) {
                        mapName = result.get();
                        //Check if the filename is already used, reloop if it is
                        check = checkIsFilenameIsUsed(mapName);
                        if (check == true) {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setContentText("File name already in use");
                            alert.showAndWait();
                        }


                    } else {
                        mapName = "";
                        break;
                    }
                }
                if (!mapName.isEmpty()) {
                    // RESET THE WORKSPACE
                    app.getWorkspaceComponent().resetLanguage();

                    // RESET THE DATA
                    app.getDataComponent().resetData();

                    //Save the file
                    File file = new File(PATH_WORK + mapName + "." + PropertiesManager.getPropertiesManager().getProperty(WORK_FILE_EXT));

                    try {
                        saveWork(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // NOW RELOAD THE WORKSPACE WITH THE RESET DATA
                    app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());

                    // MAKE SURE THE WORKSPACE IS ACTIVATED
                    app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());


                    // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE THE APPROPRIATE CONTROLS
                    AppGUI gui = app.getGUI();
                    if (gui != null) {
                        gui.updateToolbarControls(saved);
                        // TELL THE USER NEW WORK IS UNDERWAY
                        AppDialogs.showMessageDialog(gui.getWindow(), NEW_SUCCESS_TITLE,NEW_SUCCESS_AND_SAVED);
                    }

                }
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG, PROVIDE FEEDBACK
            AppDialogs.showMessageDialog(app.getGUI().getWindow(), NEW_ERROR_TITLE, NEW_ERROR_CONTENT);
        }
    }
    //This method is used to start a new file from the welcome window, thus creating a save file
    public void processNewRequestFromWelcome(String fileName){
        File file = new File(PATH_WORK + fileName + "." + PropertiesManager.getPropertiesManager().getProperty(WORK_FILE_EXT));
        try {
            saveWork(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        app.getWorkspaceComponent().resetLanguage();
        app.getDataComponent().resetData();
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
        app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
        // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE THE APPROPRIATE CONTROLS
        AppGUI gui = app.getGUI();
        if (gui != null) {
            gui.updateToolbarControls(saved);
            // TELL THE USER NEW WORK IS UNDERWAY
            AppDialogs.showMessageDialog(gui.getWindow(), NEW_SUCCESS_TITLE,NEW_SUCCESS_AND_SAVED);
        }





    }

    /**
     * This method lets the user open a Course saved to a file. It will also make sure data for the current Course is
     * not lost.
     */
    public void processLoadRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK

            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A Course
            if (continueToOpen) {
                // GO AHEAD AND PROCEED LOADING A Course
                promptToOpen();
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG
            AppDialogs.showMessageDialog(app.getGUI().getWindow(), LOAD_ERROR_TITLE, LOAD_ERROR_CONTENT);
        }
    }

    public void processCloseRequest() {
        // @todo
    }

    /**
     * This method will save the current course to a file. Note that we already know the name of the file, so we won't
     * need to prompt the user.
     */
    public void processSaveRequest() {

        // WE'LL NEED THIS TO GET CUSTOM STUFF
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        try {
            // MAYBE WE ALREADY KNOW THE FILE
            if (currentWorkFile != null) {
                saveWork(currentWorkFile);
            }
            // OTHERWISE WE NEED TO PROMPT THE USER
            else {
                // PROMPT THE USER FOR A FILE NAME
                FileChooser fc = new FileChooser();
                fc.setInitialDirectory(new File(PATH_WORK));
                fc.setTitle(props.getProperty(SAVE_WORK_TITLE));
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC),
                                                               props.getProperty(WORK_FILE_EXT)));

                File preFile = fc.showSaveDialog(app.getGUI().getWindow());
                if(preFile != null) {
                    File selectedFile = new File(preFile.getAbsolutePath() + "." + props.getProperty(WORK_FILE_EXT));
                    saveWork(selectedFile);
                }
            }
        } catch (IOException ioe) {
            AppDialogs.showMessageDialog(app.getGUI().getWindow(), SAVE_ERROR_TITLE, SAVE_ERROR_CONTENT);
        }
    }

    public void processSaveAsRequest() {
        // WE'LL NEED THIS TO GET CUSTOM STUFF
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // PROMPT THE USER FOR A FILE NAME
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(SAVE_WORK_TITLE));
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC),
                        props.getProperty(WORK_FILE_EXT)));

        File preFile = fc.showSaveDialog(app.getGUI().getWindow());
        if (preFile != null) {
            File selectedFile = new File(preFile.getAbsolutePath() + "." + props.getProperty(WORK_FILE_EXT));
            try {
                saveWork(selectedFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    public void processExportRequest(){
        String fileName = currentWorkFile.getName();
        fileName = fileName.substring(0,fileName.length() - 3);
        File file = new File(PATH_EXPORT + fileName + " Metro." + PropertiesManager.getPropertiesManager().getProperty(EXPORT_FILE_EXT));

        try {
            saveWork(file);
            app.getFileComponent().exportData(app.getDataComponent(), file.getPath());

        } catch (IOException e) {
            e.printStackTrace();
        }
        AppDialogs.showMessageDialog(app.getGUI().getWindow(),EXPORT_CONFIRMATION_TITLE, EXPORT_CONFIRMATION_CONTENT);
    }

    /**
     * This method will exit the application, making sure the user doesn't lose any data first.
     */
    public void processExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            AppDialogs.showMessageDialog(app.getGUI().getWindow(), SAVE_ERROR_TITLE, SAVE_ERROR_CONTENT);
        }
    }

    /**
     * This helper method verifies that the user really wants to save their unsaved work, which they might not want to
     * do. Note that it could be used in multiple contexts before doing other actions, like creating new work, or
     * opening another file. Note that the user will be presented with 3 options: YES, NO, and CANCEL. YES means the
     * user wants to save their work and continue the other action (we return true to denote this), NO means don't save
     * the work but continue with the other action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user presses the NO option to not save,
     *         false if the user presses the CANCEL option to not continue.
     */
    private boolean promptToSave() throws IOException {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // CHECK TO SEE IF THE CURRENT WORK HAS
        // BEEN SAVED AT LEAST ONCE

        // PROMPT THE USER TO SAVE UNSAVED WORK
        ButtonType selection = AppDialogs.showYesNoCancelDialog(app.getGUI().getWindow(),
                SAVE_VERIFY_TITLE,
                SAVE_VERIFY_CONTENT);

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection == ButtonType.YES) {

            if (currentWorkFile == null) {
                // PROMPT THE USER FOR A FILE NAME
                FileChooser fc = new FileChooser();
                fc.setInitialDirectory(new File(PATH_WORK));
                fc.setTitle(props.getProperty(SAVE_WORK_TITLE));
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC),
                                                               props.getProperty(WORK_FILE_EXT)));

                File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
                if (selectedFile != null) {
                    saveWork(selectedFile);
                    saved = true;
                }
            } else {
                saveWork(currentWorkFile);
                saved = true;
            }
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection == ButtonType.CANCEL || selection == ButtonType.NO) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected file is then loaded and the GUI updated.
     * Note that if the user cancels the open process, nothing is done. If an error occurs loading the file, a message
     * is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // WE'LL NEED TO GET CUSTOMIZED STUFF WITH THIS
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // AND NOW ASK THE USER FOR THE FILE TO OPEN
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                // RESET THE WORKSPACE
                app.getWorkspaceComponent().resetLanguage();

                // RESET THE DATA
                app.getDataComponent().resetData();

                // LOAD THE FILE INTO THE DATA
                app.getFileComponent().loadData(app.getDataComponent(), selectedFile.getAbsolutePath());

                // MAKE SURE THE WORKSPACE IS ACTIVATED
                app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());

                // AND MAKE SURE THE FILE BUTTONS ARE PROPERLY ENABLED
                currentWorkFile = selectedFile;
                saved = true;
                app.getGUI().updateToolbarControls(saved);
            } catch (Exception e) {
                AppDialogs.showMessageDialog(app.getGUI().getWindow(), LOAD_ERROR_TITLE, LOAD_ERROR_CONTENT);
            }
        }
    }
    public void loadFromWelcomeScreen(String recentWorkName){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        File recentWorkFile = new File(PATH_WORK + recentWorkName + "." + props.getProperty(WORK_FILE_EXT));
        if (recentWorkFile != null) {
            try {
                // RESET THE WORKSPACE
                app.getWorkspaceComponent().resetLanguage();

                // RESET THE DATA
                app.getDataComponent().resetData();

                // LOAD THE FILE INTO THE DATA
                app.getFileComponent().loadData(app.getDataComponent(), recentWorkFile.getAbsolutePath());

                // MAKE SURE THE WORKSPACE IS ACTIVATED
                app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());

                // AND MAKE SURE THE FILE BUTTONS ARE PROPERLY ENABLED
                currentWorkFile = recentWorkFile;
                saved = true;
                app.getGUI().updateToolbarControls(saved);
            } catch (Exception e) {

                AppDialogs.showMessageDialog(app.getGUI().getWindow(), LOAD_ERROR_TITLE, LOAD_ERROR_CONTENT);
            }
        }

    }

    // HELPER METHOD FOR SAVING WORK
    private void saveWork(File selectedFile) throws IOException {
        // SAVE IT TO A FILE
        app.getFileComponent().saveData(app.getDataComponent(), selectedFile.getPath());

        // MARK IT AS SAVED
        currentWorkFile = selectedFile;
        saved = true;

        // TELL THE USER THE FILE HAS BEEN SAVED
        AppDialogs.showMessageDialog(app.getGUI().getWindow(), SAVE_SUCCESS_TITLE, SAVE_SUCCESS_CONTENT);

        // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
        // THE APPROPRIATE CONTROLS
        AppGUI gui = app.getGUI();
        gui.updateToolbarControls(saved);
    }

    /**
     * This method marks the appropriate variable such that we know that the current Work has been edited since it's
     * been saved. The UI is then updated to reflect this.
     *
     * @param gui The user interface editing the Work.
     */
    public void markAsEdited(AppGUI gui) {
        // THE WORK IS NOW DIRTY
        saved = false;

        // LET THE UI KNOW
        //noinspection ConstantConditions
        gui.updateToolbarControls(saved);
    }

    /**
     * This mutator method marks the file as not saved, which means that when the user wants to do a file-type
     * operation, we should prompt the user to save current work first. Note that this method should be called any time
     * the course is changed in some way.
     */

    public void markFileAsNotSaved() {
        saved = false;

    }

    /**
     * Accessor method for checking to see if the current work has been saved since it was last edited.
     *
     * @return true if the current work is saved to the file, false otherwise.
     */
    @SuppressWarnings("unused")
    public boolean isSaved() {
        return saved;
    }
    public Boolean checkIsFilenameIsUsed(String fileName) {

        File folder = new File(PATH_WORK);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                String name = listOfFiles[i].getName().substring(0, listOfFiles[i].getName().length() - 3);
                if (name.equals(fileName)) {
                    return true;
                }
            }
        }
        return false;
    }

}
