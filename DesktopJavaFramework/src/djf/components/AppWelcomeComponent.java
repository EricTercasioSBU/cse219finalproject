package djf.components;

import javafx.scene.control.Hyperlink;
import javafx.scene.layout.BorderPane;

import java.util.ArrayList;

/**
 * Created by Kitcatski on 11/9/2017.
 */
public abstract class AppWelcomeComponent {
    protected BorderPane welcomePane;
    protected Hyperlink newMapLink;
    protected ArrayList<String> recentWorkNames;
    protected ArrayList<Hyperlink> recentWorkLinks;

    public BorderPane getWelcomePane(){
        return welcomePane;
    }
    public Hyperlink getNewMapLink(){
        return newMapLink;
    }
    public ArrayList<String> getRecentWorkNames(){return recentWorkNames;}
    public ArrayList<Hyperlink> getRecentWorkLinks(){return recentWorkLinks;}
}
