package met.gui;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.language.AppLanguageSettings;
import djf.ui.AppGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import met.data.*;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static djf.language.AppLanguageSettings.PATH_WORK;
import static met.css.metStyle.*;
import static met.metPropertyType.*;

/**
 * Created by Kitcatski on 11/3/2017.
 */
public class metWorkspace extends AppWorkspaceComponent {
    //The application
    AppTemplate app;

    Pane canvas;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    //Container for all of the toolbars
    VBox sideToolbar;

    //1st Row
    VBox row1Box;
    //First row of nodes within row1
    HBox row1Section1;
    Label metroLinesLabel;
    ComboBox<String> linesBox;
    Button editLineButton;
    //Second row of nodes within row1
    HBox row1Section2;
    Button addLineButton;
    Button removeLineButton;
    Button addStationToLineButton;
    Button removeStationFromLineButton;
    Button listStationsButton;
    //This one is in next row, but only 1 element so doesnt need its own HBOX
    Slider lineThicknessSlider;


    //2nd Row
    VBox row2Box;
    //First row of nodes within row2
    HBox row2Section1;
    Label metroStationLabel;
    ComboBox<String> stationsBox;
    ColorPicker stationColorButton;
    //Second row of nodes within row2
    HBox row2Section2;
    Button addStationButton;
    Button removeStationButton;
    Button snapButton;
    Button moveLabelButton;
    Button rotateButton;
    //This one is in the next row, but only 1 element so doesn't need its own HBOX
    Slider stationThicknessSlider;

    //3rd Row
    HBox row3Box;
    //VBox to hold the two ComboBox's together
    VBox findRouteVBox;
    ComboBox<String> findRouteComboBoxStarting;
    ComboBox<String> findRouteComboBoxEnding;
    //Solo element in HBOX
    Button findRouteButton;


    //4th Row
    VBox row4Box;
    //First row of nodes within row4
    HBox row4Section1;
    Label decorLabel;
    ColorPicker backgroundColorPicker;
    //Second row of nodes within row4
    HBox row4Section2;
    Button setBackgroundImageButton;
    Button addImageButton;
    Button addLabelButton;
    Button removeElementButton;

    //5th Row
    VBox row5Box;
    //First row of nodes within row5
    HBox row5Section1;
    Label fontLabel;
    ColorPicker textColorPicker;
    //Second row of nodes within row5
    HBox row5Section2;
    Button boldButton;
    Button italicButton;
    ComboBox<String> fontSizeBox;
    ComboBox<String> fontFamilyBox;

    //6th Row
    VBox row6Box;
    //First row of nodes within row6
    HBox row6Section1;
    HBox row6Section2;
    Label navigationLabel;
    RadioButton showGridButton;
    //Second row of nodes within row6
    Button zoomInButton;
    Button zoomOutButton;
    Button increaseMapButton;
    Button decreaseMapButton;

    //Invisible grid
    ArrayList<Line> grid;

    LineController lineController;

    Group gridGroup;
    ArrayList gridIntersections;
    ScrollPane scrollPane;
    Group zoomGroup;
    Group content;
    Scale scale;




    public metWorkspace(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();


    }

    private void initControllers() {
        BackgroundController backgroundController = new BackgroundController(app);
        backgroundColorPicker.setOnAction(e->{
            backgroundController.processSelectBackgroundColor();

        });
        lineController = new LineController(app,scrollPane);
        addLineButton.setOnAction(e->{
            lineController.processAddLine();
        });
        removeLineButton.setOnAction(e->{
            String selection = linesBox.getSelectionModel().getSelectedItem();
            lineController.processRemoveLine(selection);
        });
        editLineButton.setOnAction(e->{
            String selection = linesBox.getSelectionModel().getSelectedItem();
            lineController.processEditLine(selection);
        });
        addStationButton.setOnAction(e->{
            lineController.processAddStation();
        });

        removeStationButton.setOnAction(e->{
            String selection = stationsBox.getSelectionModel().getSelectedItem();
            lineController.processRemoveStation(selection);
        });
        addStationToLineButton.setOnAction(e->{
            String selection = linesBox.getSelectionModel().getSelectedItem();
            lineController.processAddStationToLine(selection);
        });
        removeStationFromLineButton.setOnAction(e->{
            String selection = linesBox.getSelectionModel().getSelectedItem();
            lineController.processRemoveStationFromLine(selection);
        });
        addLabelButton.setOnAction(e->{
            lineController.processAddLabel();
        });
        addImageButton.setOnAction(e->{
            lineController.processAddImage();
        });
        removeElementButton.setOnAction(e->{
            lineController.processRemoveElement();
        });
        textColorPicker.setOnAction(e->{
            Color color = textColorPicker.getValue();
            lineController.processChangeTextColor(color);
        });
        fontSizeBox.setOnAction(e->{
            String selection = fontSizeBox.getSelectionModel().getSelectedItem();
            lineController.processChangeTextSize(selection);
        });
        fontFamilyBox.setOnAction(e->{
            String selection = fontFamilyBox.getSelectionModel().getSelectedItem();
            lineController.processChangeTextFont(selection);
        });
        boldButton.setOnAction(e->{
            lineController.processBoldText();
        });
        italicButton.setOnAction(e->{
            lineController.processItalicText();
        });
        zoomInButton.setOnAction(e->{
            lineController.processZoomIn();
        });
        zoomOutButton.setOnAction(e->{
            lineController.processZoomOut();
        });
        showGridButton.setOnAction(e->{
            if(gridGroup == null){
                createGrid();
            }
            lineController.processShowGrid();
        });
        snapButton.setOnAction(e->{
            if(gridGroup == null){
                createGrid();
            }
            lineController.processSnapToGrid();
        });
        rotateButton.setOnAction(e->{
            String selection = stationsBox.getSelectionModel().getSelectedItem();
            lineController.processRotateStation(selection);
        });
        moveLabelButton.setOnAction(e->{
            String selection = stationsBox.getSelectionModel().getSelectedItem();
            lineController.processMoveLabel(selection);
        });
        lineThicknessSlider.valueProperty().addListener(e->{
            double value = lineThicknessSlider.getValue();
            String selection = linesBox.getSelectionModel().getSelectedItem();
            lineController.processLineThickness(value, selection);
        });
        increaseMapButton.setOnAction(e->{
            lineController.processMapIncrease();
        });
        decreaseMapButton.setOnAction(e->{
            lineController.processMapDecrease();
        });
        stationThicknessSlider.valueProperty().addListener(e->{
            double value = stationThicknessSlider.getValue();
            String selection = stationsBox.getSelectionModel().getSelectedItem();
            lineController.processStationRadius(value, selection);
        });
        setBackgroundImageButton.setOnAction(e->{
            lineController.setImageBackground();
        });
        findRouteButton.setOnAction(e->{
            String startingStation = findRouteComboBoxStarting.getSelectionModel().getSelectedItem();
            String endingStation = findRouteComboBoxEnding.getSelectionModel().getSelectedItem();
            lineController.processFindRoute(startingStation,endingStation);
        });
        listStationsButton.setOnAction(e->{
            String selection = linesBox.getSelectionModel().getSelectedItem();
            lineController.processListStations(selection);
        });
        canvas.setOnMousePressed(e->{
            lineController.processCanvasMousePress((int)e.getX(),(int)e.getY());
        });
        canvas.setOnMouseDragged(e->{
            lineController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
        });
        canvas.setOnMouseReleased(e->{
            lineController.processCanvasMouseReleased((int)e.getX(), (int)e.getY());
        });


    }

    private void initLayout() {
        //Goes on the left side (AKA the toolbar)
        sideToolbar = new VBox();


        //Row 1
        row1Box = new VBox();
        row1Section1 = new HBox();
        row1Section2 = new HBox();
        metroLinesLabel = new Label("Metro Lines");
        linesBox = new ComboBox<>();
        row1Section1.getChildren().addAll(metroLinesLabel,linesBox);
        editLineButton = gui.initPaneChildButton(row1Section1,EDIT_LINE.toString(),true);
        addLineButton = gui.initPaneChildButton(row1Section2,ADD_LINE.toString(),true);
        removeLineButton = gui.initPaneChildButton(row1Section2,REMOVE_LINE.toString(),true);
        addStationToLineButton = gui.initPaneChildButton(row1Section2,ADD_STATION_TO_LINE.toString(),true);
        removeStationFromLineButton = gui.initPaneChildButton(row1Section2,REMOVE_STATION_FROM_LINE.toString(),true);
        listStationsButton = gui.initPaneChildButton(row1Section2,LIST_STATIONS.toString(),true);
        lineThicknessSlider = new Slider();

        row1Box.getChildren().addAll(row1Section1,row1Section2,lineThicknessSlider);

        //Row 2
        row2Box = new VBox();
        row2Section1 = new HBox();
        row2Section2 = new HBox();
        metroStationLabel = new Label("Metro Stations");
        stationsBox = new ComboBox<>();
        stationColorButton = new ColorPicker();
        row2Section1.getChildren().addAll(metroStationLabel,stationsBox,stationColorButton);
        addStationButton = gui.initPaneChildButton(row2Section2,ADD_STATION.toString(),true);
        removeStationButton = gui.initPaneChildButton(row2Section2,REMOVE_STATION.toString(),true);
        snapButton = gui.initPaneChildButton(row2Section2,SNAP.toString(),true);
        snapButton.setText("Snap");
        moveLabelButton = gui.initPaneChildButton(row2Section2,MOVE_LABEL.toString(),true);
        moveLabelButton.setText("Move Label");
        rotateButton = gui.initPaneChildButton(row2Section2,ROTATE.toString(),true);
        stationThicknessSlider = new Slider();

        row2Box.getChildren().addAll(row2Section1,row2Section2,stationThicknessSlider);

        //Row 3
        row3Box = new HBox();
        findRouteVBox = new VBox();
        findRouteComboBoxStarting = new ComboBox<>();
        findRouteComboBoxEnding = new ComboBox<>();
        findRouteVBox.getChildren().addAll(findRouteComboBoxStarting,findRouteComboBoxEnding);
        row3Box.getChildren().add(findRouteVBox);
        findRouteButton = gui.initPaneChildButton(row3Box,FIND_ROUTE.toString(),true);

        //Row 4
        row4Box = new VBox();
        row4Section1 = new HBox();
        row4Section2 = new HBox();
        decorLabel = new Label("Decor");
        backgroundColorPicker = new ColorPicker();
        row4Section1.getChildren().addAll(decorLabel,backgroundColorPicker);
        setBackgroundImageButton = gui.initPaneChildButton(row4Section2,SET_BACKGROUND_IMAGE.toString(),true);
        setBackgroundImageButton.setText("Set Background Image");
        addImageButton = gui.initPaneChildButton(row4Section2,ADD_IMAGE.toString(),true);
        addImageButton.setText("Add Image");
        addLabelButton = gui.initPaneChildButton(row4Section2,ADD_LABEL.toString(),true);
        addLabelButton.setText("Add Label");
        removeElementButton = gui.initPaneChildButton(row4Section2,REMOVE_ELEMENT.toString(),true);
        removeElementButton.setText("Remove Element");
        row4Box.getChildren().addAll(row4Section1,row4Section2);

        //Row 5
        row5Box = new VBox();
        row5Section1 = new HBox();
        row5Section2 = new HBox();
        fontLabel = new Label("Font");
        textColorPicker = new ColorPicker();
        row5Section1.getChildren().addAll(fontLabel,textColorPicker);
        boldButton = gui.initPaneChildButton(row5Section2,BOLD.toString(),true);
        italicButton = gui.initPaneChildButton(row5Section2,ITALIC.toString(),true);
        ObservableList<String> fontSizeOptions =
                FXCollections.observableArrayList(
                        "6","7","8","9","10","11","12","14","16","18","20","22","24","30","36","42","48","60","72","90"
                );
        ObservableList<String> fontTypeOptions =
                FXCollections.observableArrayList(
                        "Arial","Times New Roman", "Georgia", "Comic Sans MS" , "Calibri"
                );
        fontSizeBox = new ComboBox<>(fontSizeOptions);
        fontFamilyBox = new ComboBox<>(fontTypeOptions);
        row5Section2.getChildren().addAll(fontSizeBox,fontFamilyBox);
        row5Box.getChildren().addAll(row5Section1,row5Section2);

        //Row 6
        row6Box = new VBox();
        row6Section1 = new HBox();
        row6Section2 = new HBox();
        navigationLabel = new Label("Navigation");
        showGridButton = new RadioButton("Show Grid");
        row6Section1.getChildren().addAll(navigationLabel,showGridButton);
        zoomInButton = gui.initPaneChildButton(row6Section2,ZOOM_IN.toString(),true);
        zoomOutButton = gui.initPaneChildButton(row6Section2,ZOOM_OUT.toString(),true);
        increaseMapButton = gui.initPaneChildButton(row6Section2,INCREASE.toString(),true);
        decreaseMapButton = gui.initPaneChildButton(row6Section2,DECREASE.toString(),true);
        row6Box.getChildren().addAll(row6Section1,row6Section2);





        sideToolbar.getChildren().addAll(row1Box,row2Box,row3Box,row4Box,row5Box,row6Box);

        canvas = new Pane();
        BackgroundFill fill = new BackgroundFill(Color.WHITE, null, null);
        Background background = new Background(fill);
        canvas.setBackground(background);
        metData data = (metData)app.getDataComponent();
        data.setMapNodes(canvas.getChildren());

        // AND NOW SETUP THE WORKSPACE
        workspace = new BorderPane();
        scrollPane = new ScrollPane();
        //Create 2 groups for god knows what reason
        scale = new Scale(1,1);
        zoomGroup = new Group();
        content = new Group();
        zoomGroup.getChildren().add(canvas);
        zoomGroup.getTransforms().add(scale);
        content.getChildren().add(zoomGroup);
        //Add pane to scrollpane
        scrollPane.setContent(content);
        ((BorderPane)workspace).setCenter(scrollPane);
        ((BorderPane)workspace).setLeft(sideToolbar);
        canvas.setPrefHeight(1000);
        canvas.setPrefWidth(1500);




        // GIVE THE LABELS TO THE LANGUAGE MANAGER
//        AppLanguageSettings language = app.getLanguageSettings();
//        language.addLabeledControl(BACKGROUND_COLOR, backgroundColorLabel);
//        language.addLabeledControl(FILL_COLOR, fillColorLabel);
//        language.addLabeledControl(OUTLINE_COLOR, outlineColorLabel);
//        language.addLabeledControl(OUTLINE_THICKNESS, outlineThicknessLabel);



    }
    private void initStyle(){

        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
        row1Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row1Section1.getStyleClass().add(INNER_TOOLBAR_ROW);
        row1Section2.getStyleClass().add(INNER_TOOLBAR_ROW);
        row2Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row2Section1.getStyleClass().add(INNER_TOOLBAR_ROW);
        row2Section2.getStyleClass().add(INNER_TOOLBAR_ROW);
        row3Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row4Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row4Section1.getStyleClass().add(INNER_TOOLBAR_ROW);
        row4Section2.getStyleClass().add(INNER_TOOLBAR_ROW);
        row5Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row5Section1.getStyleClass().add(INNER_TOOLBAR_ROW);
        row5Section2.getStyleClass().add(INNER_TOOLBAR_ROW);
        row6Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row6Section1.getStyleClass().add(INNER_TOOLBAR_ROW);
        row6Section2.getStyleClass().add(INNER_TOOLBAR_ROW);
        stationThicknessSlider.setMaxWidth(300);
        lineThicknessSlider.setMaxWidth(300);
        stationsBox.setMinWidth(200);
        linesBox.setMinWidth(200);
        findRouteComboBoxStarting.setMinWidth(200);
        findRouteComboBoxEnding.setMinWidth(200);
        editLineButton.getStyleClass().add(CLASS_BUTTON);
        backgroundColorPicker.getStyleClass().add(CLASS_BUTTON);
        stationColorButton.getStyleClass().add(CLASS_BUTTON);
        textColorPicker.getStyleClass().add(CLASS_BUTTON);
        addLabelButton.setWrapText(true);
        addLabelButton.getStyleClass().add(TWO_LINE_BUTTON);
        sideToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);


    }


    @Override
    public void resetLanguage() {

    }

    @Override
    public void reloadWorkspace(AppDataComponent data) {
        metData dataManager = (metData)data;
        if (dataManager.isInState(metState.STARTING_LINE)) {
//            selectionToolButton.setDisable(false);
//            removeButton.setDisable(true);
//            rectButton.setDisable(true);
//            ellipseButton.setDisable(false);
        }

        else if (dataManager.isInState(metState.SELECTING_NODE)
                || dataManager.isInState(metState.DRAGGING_NODE)
                || dataManager.isInState(metState.DRAGGING_NOTHING)) {
//            boolean nodeIsNotSelected = dataManager.getSelectedNode() == null;
//            selectionToolButton.setDisable(true);
//            removeButton.setDisable(nodeIsNotSelected);
//            rectButton.setDisable(false);
//            ellipseButton.setDisable(false);
//            moveToFrontButton.setDisable(nodeIsNotSelected);
//            moveToBackButton.setDisable(nodeIsNotSelected);
        }

//        removeButton.setDisable(dataManager.getSelectedNode() == null);
        linesBox.getItems().clear();
        linesBox.getItems().addAll(dataManager.getLineNames());
        stationsBox.getItems().clear();
        stationsBox.getItems().addAll(dataManager.getStationNames());
        findRouteComboBoxStarting.getItems().clear();
        findRouteComboBoxStarting.getItems().addAll(dataManager.getStationNames());
        findRouteComboBoxEnding.getItems().clear();
        findRouteComboBoxEnding.getItems().addAll(dataManager.getStationNames());

    }

    public Pane getCanvas(){
        return canvas;
    }
    public ColorPicker getBackgroundColorPicker(){
        return backgroundColorPicker;
    }

    // HELPER METHOD
//    public void loadSelectedNodeSettings(Node node) {
//        if (node != null) {
//            metData data = (metData)app.getDataComponent();
//            if (data.isShape((Draggable)node)) {
//                Shape shape = (Shape)node;
//                Color fillColor = (Color)shape.getFill();
//                Color strokeColor = (Color)shape.getStroke();
//                double lineThickness = shape.getStrokeWidth();
////                fillColorPicker.setValue(fillColor);
////                outlineColorPicker.setValue(strokeColor);
////                outlineThicknessSlider.setValue(lineThickness);
//            }
//        }
//    }
    public ComboBox getLinesBox(){
        return linesBox;
    }
    public LineController getLineController(){
        return lineController;
    }
    public void createGrid(){
        int size = 15;
        int widthSize = (int) (canvas.getWidth() / size);
        int heightSize = (int) (canvas.getHeight() / size);
        GridGroup group = new GridGroup();
        ArrayList<Line> xLines = new ArrayList<>();
        ArrayList<Line> yLines = new ArrayList<>();
        ArrayList<Point> intersections = new ArrayList<>();
        //Start with the Vertical Lines
        for (int i = 1; i < widthSize; i++) {
            DraggableLine line = new DraggableLine(i * size, 0, i * size, canvas.getHeight());
            group.getChildren().add(line);
            xLines.add(line);
        }
        //Then with the horizontal
        for (int i = 1; i < heightSize; i++) {
            DraggableLine line = new DraggableLine(0, i * size, canvas.getWidth(), i * size);
            group.getChildren().add(line);
            yLines.add(line);
        }
        //Now check for intersections to save
        for (int i = 0; i < xLines.size(); i++){
            Line xLine = xLines.get(i);
            int xStartX = (int) xLine.getStartX();
            int xStartY = (int) xLine.getStartY();
            int xEndX = (int) xLine.getEndX();
            int xEndY = (int) xLine.getEndY();
            for(int k = 0; k < yLines.size(); k++){
                Line yLine = yLines.get(k);
                int yStartX = (int) yLine.getStartX();
                int yStartY = (int) yLine.getStartY();
                int yEndX = (int) yLine.getEndX();
                int yEndY = (int) yLine.getEndY();
                Point point = lineIntersect(xStartX,xStartY,xEndX,xEndY,yStartX,yStartY,yEndX,yEndY);
                intersections.add(point);
            }
        }
        gridIntersections = intersections;
        gridGroup = group;
        gridGroup.setVisible(false);
        canvas.getChildren().add(gridGroup);
    }
    public Group getGridGroup(){
        return gridGroup;
    }
    public static Point lineIntersect(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
        double denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
        if (denom == 0.0) { // Lines are parallel.
            return null;
        }
        double ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3))/denom;
        double ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3))/denom;
        if (ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f) {
            // Get the intersection point.
            return new Point((int) (x1 + ua*(x2 - x1)), (int) (y1 + ua*(y2 - y1)));
        }

        return null;
    }


    public ArrayList getGridIntersections() {
        return gridIntersections;
    }

    public Scale getScale() {
        return scale;
    }


    public ScrollPane getScrollPane() {
        return scrollPane;
    }
}

