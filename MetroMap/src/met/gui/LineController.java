package met.gui;

import djf.AppTemplate;
import djf.ui.AppDialogs;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import jtps.jTPS;
import met.data.*;
import met.transactions.*;
import properties_manager.PropertiesManager;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import static djf.language.AppLanguageSettings.FILE_PROTOCOL;
import static djf.language.AppLanguageSettings.PATH_IMAGES;
import static met.data.metState.*;
import static met.metPropertyType.DEFAULT_NODE_X;
import static met.metPropertyType.DEFAULT_NODE_Y;


/**
 * Created by Kitcatski on 11/13/2017.
 */
public class LineController {
    AppTemplate app;
    metData dataManager;
    String currentLineName;
    String currentStationName;
    String stationToAddLineTo;
    String lineToRemoveStationFrom;
    String currentLabelText;
    Color currentLineColor;
    ArrayList<Route> routes;
    String imageUrl;

    public LineController(AppTemplate initApp, ScrollPane scrollPane) {
        app = initApp;
        dataManager = (metData) app.getDataComponent();
        initKeyboardControls(scrollPane);
    }

    public void processRemoveLine(String lineToDelete) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Remove Line");
        alert.setHeaderText("Removing Line");
        alert.setContentText("Are you sure you want to remove this line?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            dataManager.removeLine(lineToDelete);
            metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);
        } else {

        }

    }

    public void initKeyboardControls(ScrollPane scrollPane) {
        Scene scene = app.getGUI().getPrimaryScene();

        scene.setOnKeyPressed(e -> {
            double vvalueProperty = scrollPane.vvalueProperty().getValue();
            if (e.getCode().equals(KeyCode.W)) {
                scrollPane.vvalueProperty().setValue(scrollPane.getVvalue() - .05);
            } else if (e.getCode().equals(KeyCode.A)) {
                scrollPane.hvalueProperty().setValue(scrollPane.getHvalue() - .05);

            } else if (e.getCode().equals(KeyCode.S)) {
                scrollPane.vvalueProperty().setValue(scrollPane.getVvalue() + .05);

            } else if (e.getCode().equals(KeyCode.D)) {
                scrollPane.hvalueProperty().setValue(scrollPane.getHvalue() + .05);

            }
        });
    }

    public void processAddLine() {
        //Open line dialog
        AddLineDialog stationDialog = new AddLineDialog();
        Button okayButton = stationDialog.getOkayButton();
        Button cancelButton = stationDialog.getCancelButton();
        okayButton.setOnAction(e -> {
            String nameResult = stationDialog.getNameResult();
            Color colorResult = stationDialog.getColorResult();

            if (!nameResult.isEmpty()) {
                currentLineName = nameResult;
                currentLineColor = colorResult;
                // CHANGE THE CURSOR
                Scene scene = app.getGUI().getPrimaryScene();
                scene.setCursor(Cursor.CROSSHAIR);

                // CHANGE THE STATE
                dataManager.setState(metState.STARTING_LINE);

                // ENABLE/DISABLE THE PROPER BUTTONS
                metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
                workspace.reloadWorkspace(dataManager);
                stationDialog.close();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Please enter a name");
                alert.showAndWait();
            }
        });
        cancelButton.setOnAction(e -> {
            stationDialog.close();

        });

    }

    public void processEditLine(String lineName) {
        if (lineName != null) {

            dataManager.editLine(lineName);

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please select a line");
            alert.showAndWait();
        }
        metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processAddStation() {
        //Open text dialog
        TextInputDialog textInputDialog = new TextInputDialog();
        textInputDialog.setContentText("Enter the name of the station");
        Optional<String> result = textInputDialog.showAndWait();
        if (result.isPresent()) {
            currentStationName = result.get();
            // CHANGE THE CURSOR
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.CROSSHAIR);

            // CHANGE THE STATE
            dataManager.setState(metState.STARTING_STATION);

            // ENABLE/DISABLE THE PROPER BUTTONS
            metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);
        }
    }

    public void processRemoveStation(String stationName) {
        dataManager.removeStation(stationName);
        metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processCanvasMousePress(int x, int y) {

        metData dataManager = (metData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_NODE)) {
            //Check to see if line end is selected
            Node node = dataManager.selectTopNode(x, y);
            // SELECT THE TOP NODE
//            Node node = dataManager.selectTopNode(x, y);
//            Scene scene = app.getGUI().getPrimaryScene();

            // AND START DRAGGING IT
//            if (node != null) {
//                scene.setCursor(Cursor.MOVE);
//                dataManager.setState(golState.DRAGGING_NODE);
//                app.getGUI().updateToolbarControls(false);
//            } else {
//                scene.setCursor(Cursor.DEFAULT);
//                dataManager.setState(DRAGGING_NOTHING);
//                app.getWorkspaceComponent().reloadWorkspace(dataManager);
//            }
        } else if (dataManager.isInState(metState.STARTING_LINE)) {
            dataManager.startNewLine(currentLineName, currentLineColor, x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            dataManager.setState(SELECTING_NODE);
        } else if (dataManager.isInState(metState.STARTING_STATION)) {
            dataManager.startNewStation(currentStationName, x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            dataManager.setState(SELECTING_NODE);
        } else if (dataManager.isInState(ADDING_STATION_TO_LINE)) {
            Node node = dataManager.getTopNode(x, y);
            if (node instanceof DraggableGroup) {
                dataManager.addStationToLine(stationToAddLineTo, node);
            } else {
                Scene scene = app.getGUI().getPrimaryScene();
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(SELECTING_NODE);
            }

        } else if (dataManager.isInState(REMOVING_STATION_FROM_LINE)) {
            Node node = dataManager.getTopNode(x, y);
            if (node instanceof DraggableGroup) {
                dataManager.removeStationFromLine(lineToRemoveStationFrom, (DraggableGroup) node);

            } else {
                Scene scene = app.getGUI().getPrimaryScene();
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(SELECTING_NODE);
            }

        } else if (dataManager.isInState(SNAPPING_TO_GRID)) {
            Node node = dataManager.selectTopNode(x, y);
            if (node instanceof Circle || node instanceof DraggableText) {
                dataManager.snapToGrid(node);
                Scene scene = app.getGUI().getPrimaryScene();
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(SELECTING_NODE);
            }

        } else if (dataManager.isInState(STARTING_LABEL)) {
            dataManager.startNewLabel(currentLabelText, x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            dataManager.setState(SELECTING_NODE);


            metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);
        }
    }

    public void processCanvasMouseDragged(int x, int y) {
        metState state = dataManager.getState();
        metData dataManager = (metData) app.getDataComponent();
        if (dataManager.isInState(SIZING_LINE_END) || dataManager.isInState(SELECTING_NODE)) {
            Draggable selectedDraggableNode = (Draggable) dataManager.getSelectedNode();
            if (selectedDraggableNode instanceof DraggableText && ((DraggableText) selectedDraggableNode).getParent() instanceof DraggableGroup) {

                DraggableGroup parent = (DraggableGroup) ((DraggableText) selectedDraggableNode).getParent();
                if (parent.getChildren().get(0).equals(selectedDraggableNode)) {
                    selectedDraggableNode.drag(x - (int) (selectedDraggableNode.getWidth()), y);
//                    line.setStartX(x); For QuadCurve
//                    line.setStartY(y);
                } else {
                    selectedDraggableNode.drag(x + (int) (selectedDraggableNode.getWidth()), y);
//                    line.setEndX(x); For QuadCurve
//                    line.setEndY(y);
                }
            } else if (selectedDraggableNode instanceof DraggableStation || selectedDraggableNode instanceof DraggableText || selectedDraggableNode instanceof DraggableImage) {
                selectedDraggableNode.drag(x, y);
            }
        }
        dataManager.setState(state);


        app.getGUI().updateToolbarControls(false);
    }

    public void processCanvasMouseReleased(int x, int y) {
        metData dataManager = (metData) app.getDataComponent();
        if (dataManager.isInState(SIZING_LINE_END)) {
            dataManager.setState(SELECTING_NODE);
            app.getGUI().updateToolbarControls(false);
        }

    }

    public void processAddStationToLine(String selection) {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);
        dataManager.setState(ADDING_STATION_TO_LINE);
        stationToAddLineTo = selection;

    }

    public void processRemoveStationFromLine(String selection) {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);
        dataManager.setState(REMOVING_STATION_FROM_LINE);
        lineToRemoveStationFrom = selection;
    }

    public Color getCurrentLineColor() {
        return currentLineColor;
    }

    public void processAddLabel() {
        //Open text dialog
        TextInputDialog textInputDialog = new TextInputDialog();
        textInputDialog.setContentText("Enter label text");
        Optional<String> result = textInputDialog.showAndWait();
        if (result.isPresent()) {
            currentLabelText = result.get();
            // CHANGE THE CURSOR
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.CROSSHAIR);

            // CHANGE THE STATE
            dataManager.setState(metState.STARTING_LABEL);

            // ENABLE/DISABLE THE PROPER BUTTONS
            metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);
        }


    }

    public void processAddImage() {
        // ASK THE USER TO SELECT AN IMAGE
        Image imageToAdd = promptForImage();
        if (imageToAdd != null) {
            DraggableImage imageViewToAdd = new DraggableImage(imageUrl);
            imageViewToAdd.setImage(imageToAdd);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            imageViewToAdd.xProperty().set(Double.parseDouble(props.getProperty(DEFAULT_NODE_X)));
            imageViewToAdd.yProperty().set(Double.parseDouble(props.getProperty(DEFAULT_NODE_Y)));

            // MAKE AND ADD THE TRANSACTION
            addNodeTransaction(imageViewToAdd);

        }
    }

    private Image promptForImage() {
        // SETUP THE FILE CHOOSER FOR PICKING IMAGES
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(PATH_IMAGES));
        FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
        FileChooser.ExtensionFilter extFilterGIF = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterBMP, extFilterGIF, extFilterJPG, extFilterPNG);
        fileChooser.setSelectedExtensionFilter(extFilterPNG);

        // OPEN THE DIALOG
        File file = fileChooser.showOpenDialog(null);
        try {
            if (file != null) {
                imageUrl = FILE_PROTOCOL +file.getPath();
                BufferedImage bufferedImage = ImageIO.read(file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                return image;
            }
        } catch (IOException ex) {
            AppDialogs.showMessageDialog(app.getGUI().getWindow(), "ERROR LOADING IMAGE TITLE", "ERROR LOADING IMAGE CONTENT");
            return null;
        }
        return null;
    }

    private void addNodeTransaction(Node nodeToAdd) {
        metData data = (metData) app.getDataComponent();
        AddNode_Transaction transaction = new AddNode_Transaction(data, nodeToAdd);
        jTPS tps = app.getTPS();
        tps.addTransaction(transaction);
    }

    public void processRemoveElement() {
        Node node = dataManager.getSelectedNode();
        if (node == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please select a map element");
            alert.showAndWait();
        } else if (!(node.getParent() instanceof DraggableGroup)) {
            dataManager.removeNode(node);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please select an element that is not a station or a line");
            alert.showAndWait();
        }
    }

    public void processChangeTextColor(Color color) {
        Node node = dataManager.getSelectedNode();
        if (node instanceof DraggableText) {
            ((DraggableText) node).setStroke(color);
        } else if (node instanceof DraggableStation) {
            DraggableGroup parent = (DraggableGroup) node.getParent();
            DraggableText text = (DraggableText) parent.getChildren().get(1);
            text.setStroke(color);
        }
    }

    public void processChangeTextSize(String selection) {
        Node node = dataManager.getSelectedNode();
        int textSize = Integer.parseInt(selection);
        if (node instanceof DraggableText) {
            DraggableText text = (DraggableText) node;
            TextSize_Transaction textSizeTransaction = new TextSize_Transaction(text, textSize);
            jTPS tps = app.getTPS();
            tps.addTransaction(textSizeTransaction);

        } else if (node instanceof DraggableStation) {
            DraggableGroup parent = (DraggableGroup) node.getParent();
            DraggableText text = (DraggableText) parent.getChildren().get(1);
            TextSize_Transaction textSizeTransaction = new TextSize_Transaction(text, textSize);
            jTPS tps = app.getTPS();
            tps.addTransaction(textSizeTransaction);
        }
    }

    public void processChangeTextFont(String selection) {
        Node node = dataManager.getSelectedNode();
        if (node instanceof DraggableText) {
            DraggableText text = (DraggableText) node;
            TextFont_Transaction textFontTransaction = new TextFont_Transaction(text, selection);
            jTPS tps = app.getTPS();
            tps.addTransaction(textFontTransaction);

        } else if (node instanceof DraggableStation) {
            DraggableGroup parent = (DraggableGroup) node.getParent();
            DraggableText text = (DraggableText) parent.getChildren().get(1);
            TextFont_Transaction textFontTransaction = new TextFont_Transaction(text, selection);
            jTPS tps = app.getTPS();
            tps.addTransaction(textFontTransaction);
        }
    }

    public void processItalicText() {
        Node node = dataManager.getSelectedNode();
        if (node instanceof DraggableText) {
            DraggableText text = (DraggableText) node;
            ItalicText_Transaction italicText_Transaction = new ItalicText_Transaction(text);
            jTPS tps = app.getTPS();
            tps.addTransaction(italicText_Transaction);

        } else if (node instanceof DraggableStation) {
            DraggableGroup parent = (DraggableGroup) node.getParent();
            DraggableText text = (DraggableText) parent.getChildren().get(1);
            ItalicText_Transaction italicText_Transaction = new ItalicText_Transaction(text);
            jTPS tps = app.getTPS();
            tps.addTransaction(italicText_Transaction);
        }
    }

    public void processBoldText() {
        Node node = dataManager.getSelectedNode();
        if (node instanceof DraggableText) {
            DraggableText text = (DraggableText) node;
            BoldText_Transaction boldText_Transaction = new BoldText_Transaction(text);
            jTPS tps = app.getTPS();
            tps.addTransaction(boldText_Transaction);

        } else if (node instanceof DraggableStation) {
            DraggableGroup parent = (DraggableGroup) node.getParent();
            DraggableText text = (DraggableText) parent.getChildren().get(1);
            BoldText_Transaction boldText_Transaction = new BoldText_Transaction(text);
            jTPS tps = app.getTPS();
            tps.addTransaction(boldText_Transaction);
        }
    }

    public void processZoomIn() {
        Scale scale = ((metWorkspace) app.getWorkspaceComponent()).getScale();
        scale.setX(scale.getX() * 1.1);
        scale.setY(scale.getY() * 1.1);
    }

    public void processZoomOut() {
        Scale scale = ((metWorkspace) app.getWorkspaceComponent()).getScale();
        scale.setX(scale.getX() * .9);
        scale.setY(scale.getY() * .9);

    }

    public void processMapIncrease() {
        Pane canvas = dataManager.getCanvas();
        int newHeight = (int) (canvas.getHeight() * 1.1);
        int newWidth = (int) (canvas.getWidth() * 1.1);
        canvas.setMinHeight(newHeight);
        canvas.setMaxHeight(newHeight);
        canvas.setMinWidth(newWidth);
        canvas.setMaxWidth(newWidth);
        dataManager.recreateGrid();

    }

    public void processMapDecrease() {
        Pane canvas = dataManager.getCanvas();
        int newHeight = (int) (canvas.getHeight() * .9);
        int newWidth = (int) (canvas.getWidth() * .9);
        canvas.setMinHeight(newHeight);
        canvas.setMaxHeight(newHeight);
        canvas.setMinWidth(newWidth);
        canvas.setMaxWidth(newWidth);
        dataManager.recreateGrid();
    }


    public void processShowGrid() {
        Group gridGroup = ((metWorkspace) app.getWorkspaceComponent()).getGridGroup();
        for (int i = 0; i < dataManager.getCanvas().getChildren().size(); i++) {
            if (dataManager.getCanvas().getChildren().get(i).equals(gridGroup)) {
                if (dataManager.getCanvas().getChildren().get(i).isVisible()) {
                    dataManager.getCanvas().getChildren().get(i).setVisible(false);
                } else {
                    dataManager.getCanvas().getChildren().get(i).setVisible(true);
                }
            }
        }

    }

    public void processSnapToGrid() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(metState.SNAPPING_TO_GRID);

        // ENABLE/DISABLE THE PROPER BUTTONS
        metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processRotateStation(String selection) {
        dataManager.rotateStation(selection);
        metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processLineThickness(double value, String lineName) {
        dataManager.changeLineThickness(value, lineName);

    }

    public void processStationRadius(double value, String selection) {
        dataManager.changeStationRadius(value, selection);
    }

    public void setImageBackground() {
        Image image = promptForImage();
        Pane canvas = dataManager.getCanvas();
        BackgroundSize backgroundSize = new BackgroundSize(canvas.getWidth(), canvas.getHeight(), true, true, true, false);
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        canvas.setBackground(new Background(backgroundImage));
        dataManager.setBackgroundImagePath(imageUrl);
    }

    public void processFindRoute(String startingStation, String endingStation) {
        dataManager.findConnections();
        ArrayList<DraggableGroup> routeStations = new ArrayList<>();
        //First find the station
        DraggableGroup startStation = dataManager.findStationByName(startingStation);
        DraggableGroup endStation = dataManager.findStationByName(endingStation);
        //Lets get the lineGroup of both stations, it'll probably find use
        DraggableGroup startLineGroup = (DraggableGroup) startStation.getParent();
        DraggableGroup endLineGroup = (DraggableGroup) endStation.getParent();
        //First lets check if they're on the same line, if they are, finding the route will be easy
        if (startLineGroup.equals(endLineGroup)) {
            int increment = -2;
            int startPosition = startLineGroup.getChildren().indexOf(startStation);
            int endPosition = startLineGroup.getChildren().indexOf(endStation);
            if (startPosition < endPosition) {
                increment = +2;
            }
            while (startPosition != endPosition) {
                DraggableGroup station = (DraggableGroup) startLineGroup.getChildren().get(startPosition);
                routeStations.add(station);
                startPosition = startPosition + increment;
            }
            routeStations.add(endStation);
            displayRoute(routeStations);
        }else {
            //getStationsFromConnection(startStation);
            //routeStations = stationRoute(startStation,endStation);
            //displayRoute(routeStations);
            Route route = new Route();
            routes = new ArrayList<>();
            route.addStation(startStation);
            newStart(startStation, endStation, route);
            Route newRoute = null;
            if(!routes.isEmpty()) {
                for(int i = 0; i < routes.size(); i++){
                    Route someRoute = routes.get(i);
                    someRoute.displayStations();
                    if(someRoute.lastStation().equals(endStation)){
                        newRoute = someRoute;
                    }
                }
                displayRoute(newRoute);
            }
        }
    }

    public Route newStart(DraggableGroup startStation, DraggableGroup endStation, Route route) {
        DraggableGroup startLineGroup = (DraggableGroup) startStation.getParent();
        //First things first, check if the parent of the start station (AKA LINE) contains the endstation
        if (startLineGroup.getChildren().contains(endStation)) {
            if (startLineGroup.getChildren().indexOf(startStation) < startLineGroup.getChildren().indexOf(endStation)) {
                int startIndex = startLineGroup.getChildren().indexOf(startStation);
                int endIndex = startLineGroup.getChildren().indexOf(endStation);
                if(startIndex == endIndex){
                    route.addStation(endStation);
                }else {
                    for (int i = startIndex; i < endIndex; i++) {
                        if (startLineGroup.getChildren().get(i) instanceof DraggableGroup) {
                            route.addStation((DraggableGroup) startLineGroup.getChildren().get(i));
                        }
                    }
                }
            } else {
                int startIndex = startLineGroup.getChildren().indexOf(endStation);
                int endIndex = startLineGroup.getChildren().indexOf(startStation);
                if(startIndex == endIndex){
                    route.addStation(endStation);
                }else {
                    for (int i = startIndex; i < endIndex; i++) {
                        if (startLineGroup.getChildren().get(i) instanceof DraggableGroup) {
                            route.addStation((DraggableGroup) startLineGroup.getChildren().get(i));
                        }
                    }
                }
            }
            routes.add(route);
            return route;

        }
        //If not on this line, then check for connections
        int startIndex = startLineGroup.getChildren().indexOf(startStation);
        ArrayList<DraggableGroup> stationsThatHaveConnections = new ArrayList<>();
        for(int i = startIndex; i < startLineGroup.getChildren().size(); i++){
            if(startLineGroup.getChildren().get(i) instanceof DraggableGroup &&
                    ((DraggableGroup)startLineGroup.getChildren().get(i)).getConnection() != null){
                stationsThatHaveConnections.add((DraggableGroup) startLineGroup.getChildren().get(i));
            }
        }
        //Now check if the ones before have connections too
        for(int i = startIndex; i > 0; i--) {
            if (startLineGroup.getChildren().get(i) instanceof DraggableGroup &&
                    ((DraggableGroup) startLineGroup.getChildren().get(i)).getConnection() != null) {
                stationsThatHaveConnections.add((DraggableGroup) startLineGroup.getChildren().get(i));
            }
        }
        //Now create routes for those with connections
        for(int i = 0; i < stationsThatHaveConnections.size(); i++){
            int index = startLineGroup.getChildren().indexOf(stationsThatHaveConnections.get(i));
            Route newRoute = new Route();
            if(startIndex < index){
                for(int k = startIndex; k <= index; k++ ){
                    if(startLineGroup.getChildren().get(k) instanceof DraggableGroup){
                        newRoute.addStation((DraggableGroup) startLineGroup.getChildren().get(k));
                    }
                }
            }else{
                for(int k = startIndex; k >= index; k-- ){
                    if(startLineGroup.getChildren().get(k) instanceof DraggableGroup){
                        newRoute.addStation((DraggableGroup) startLineGroup.getChildren().get(k));
                    }
                }

            }
            DraggableGroup connectedStation = stationsThatHaveConnections.get(i);
            ArrayList<DraggableGroup> nextLineStations = getStationsFromConnection(connectedStation);
            Route temp = newRoute;
            for(int z = 0; z < nextLineStations.size(); z++) {
                newRoute = newStart(nextLineStations.get(z), endStation, newRoute);
                if(newRoute.lastStation().equals(endStation)){
                    return newRoute;
                }
                newRoute = temp;
            }

        }
        return null;
        }

    public void stationRouteRecursion(){

    }
    public void displayRoute(ArrayList<DraggableGroup> route){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Route");
        alert.setHeaderText("Found route");
        String message = new String();
        for(int i = 0; i < route.size(); i++){
            System.out.println(i);
            String stationName = ((Text)route.get(i).getChildren().get(1)).getText();
            if(i == 0){
                message = message.concat("\t\t\t" +stationName);
            }else
            message = message.concat( "\n\t\t\tTO\n\t\t\t"+stationName);
        }
        alert.setContentText(message);
        alert.showAndWait();

    }
    public void displayRoute(Route route){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Route");
        alert.setHeaderText("Found route");
        String message = new String();
        Node parent = route.getStations().get(0).getParent();
        for(int i = 0; i < route.getStations().size(); i++){
            DraggableGroup station = route.getStations().get(i);
            String stationName = ((Text)station.getChildren().get(1)).getText();
            Text text = (Text) ((DraggableGroup)station.getParent()).getChildren().get(0);
            String lineName = text.getText();
            if(i == 0){
                message = message.concat("\t\t\t\t" +stationName +"\n");
            }
            else if(station.getParent() != parent){
                message = message.concat("\n\t\t\t*TRANSFER TO "+lineName +"* \n\n\t\t\t\t" +stationName );
            }else {
                message = message.concat("\n\t\t\t\t   *TO* \n\n\t\t\t\t" +stationName +"\n");
            }
            parent = station.getParent();
        }
        alert.setContentText(message);
        alert.showAndWait();

    }
    public ArrayList<DraggableGroup> getStationsFromConnection(DraggableGroup group) {
        ArrayList<DraggableGroup> nextStations = new ArrayList<>();
        DraggableGroup lineConnection = group.getConnection();
        DraggableGroup leftStation;
        DraggableGroup rightStation;
        DraggableLine line = null;
        for (int i = 0; i < lineConnection.getChildren().size(); i++) {
            if (lineConnection.getChildren().get(i).contains(group.getConnectionX(), group.getConnectionY())) {
                line = (DraggableLine) lineConnection.getChildren().get(i);
            }
        }
        int linePosition = lineConnection.getChildren().indexOf(line);
        if (lineConnection.getChildren().get(linePosition + 1) instanceof DraggableGroup) {
            rightStation = (DraggableGroup) lineConnection.getChildren().get(linePosition + 1);
            nextStations.add(rightStation);
        }
        if (lineConnection.getChildren().get(linePosition - 1) instanceof DraggableGroup) {
            leftStation = (DraggableGroup) lineConnection.getChildren().get(linePosition - 1);
            nextStations.add(leftStation);
        }
//        System.out.println("Next Stations!");
//        System.out.println(((Text)nextStations.get(0).getChildren().get(1)).getText());
//        System.out.println(((Text)nextStations.get(1).getChildren().get(1)).getText());
        return nextStations;
    }

    public void processMoveLabel(String selection) {
        dataManager.moveLabel(selection);
    }

    public void processListStations(String selection) {
        DraggableGroup line = dataManager.getLineFromString(selection);
        ArrayList<DraggableGroup> stations = dataManager.getStationsInLine(line);
        String message = new String("\t\t| STATIONS |");
        for(int i = 0; i < stations.size();i++){
            DraggableGroup station = stations.get(i);
            Text text = (Text) station.getChildren().get(1);
            message = message.concat("\n\t\t"+text.getText());
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Stations in line");
        alert.setContentText(message);
        alert.showAndWait();
    }
}
