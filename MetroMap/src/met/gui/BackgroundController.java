package met.gui;

import djf.AppTemplate;
import djf.ui.FileController;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import jtps.jTPS;
import met.transactions.ChangeBackgroundColor_Transaction;

/**
 * Created by Eric on 11/11/2017.
 */
public class BackgroundController {
        AppTemplate app;

        public BackgroundController(AppTemplate initApp) {
            app = initApp;
        }

        /**
         * This method processes a user request to select the
         * background color.
         */
        public void processSelectBackgroundColor() {
            metWorkspace workspace = (metWorkspace)app.getWorkspaceComponent();
            Color selectedColor = workspace.getBackgroundColorPicker().getValue();
            if (selectedColor != null) {
                Pane canvas = workspace.getCanvas();
                ChangeBackgroundColor_Transaction transaction = new ChangeBackgroundColor_Transaction(canvas, selectedColor);
                jTPS tps = app.getTPS();
                tps.addTransaction(transaction);
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();

            }
        }


}
