package met.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by Kitcatski on 11/24/2017.
 */
public class AddLineDialog {
    //Stage
    Stage stage;
    // panes/boxes
    private BorderPane pane;
    private HBox nameBox;
    private VBox nameAndColorBox;
    private HBox buttonBox;
    private HBox newLineBox;
    private HBox colorBox;
    //Text
    private Text newLineText;
    private Text name;
    private Text colorText;
    //Inputs
    private TextField nameField;
    private ColorPicker colorPicker;
    //Buttons
    private Button okayButton;
    private Button cancelButton;


    public AddLineDialog(){
        initLayout();
    }

    private void initLayout() {
        pane = new BorderPane();
        nameBox = new HBox();
        nameAndColorBox = new VBox();
        buttonBox = new HBox();
        newLineBox = new HBox();
        colorBox = new HBox();

        newLineText = new Text("New Line");
        newLineText.setFont(new Font("Arial",25));

        newLineBox.getChildren().add(newLineText);
        newLineBox.setAlignment(Pos.CENTER);
        newLineBox.setPadding(new Insets(10,0,0,0));
        name = new Text("Line name : ");
        colorText = new Text("Fill color : ");
        nameField = new TextField();
        colorPicker = new ColorPicker(Color.BLACK);

        okayButton = new Button("Confirm");
        cancelButton = new Button("Cancel");

        nameBox.getChildren().addAll(name, nameField);
        nameBox.setSpacing(30);
        nameBox.setAlignment(Pos.CENTER);
        colorBox.getChildren().addAll(colorText,colorPicker);
        colorBox.setSpacing(30);
        colorBox.setAlignment(Pos.CENTER);
        nameAndColorBox.getChildren().addAll(nameBox,colorBox);
        nameAndColorBox.setAlignment(Pos.CENTER);
        nameAndColorBox.setSpacing(10);


        buttonBox.getChildren().addAll(okayButton,cancelButton);
        buttonBox.setSpacing(10);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
        buttonBox.setPadding(new Insets(0,10,10,0));

        pane.setTop(newLineBox);
        pane.setCenter(nameAndColorBox);
        pane.setBottom(buttonBox);


        stage = new Stage();
        stage.setScene(new Scene(pane,400,400));

        stage.show();
    }
    public String getNameResult(){
        return nameField.getText();
    }
    public Color getColorResult(){
        return colorPicker.getValue();
    }
    public Button getOkayButton(){
        return okayButton;
    }
    public Button getCancelButton(){
        return cancelButton;
    }
    public void close(){
        stage.close();
    }
    public void setCurrentText(String text){
        nameField.setText(text);
    }
    public void setCurrentColor(Color color){
        colorPicker.setValue(color);
    }
    public void setDialogContextText(String text){
        newLineText.setText(text);
    }

}
