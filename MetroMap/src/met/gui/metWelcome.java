package met.gui;

import djf.AppTemplate;
import djf.components.AppWelcomeComponent;
import djf.ui.AppGUI;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;

import static djf.language.AppLanguageSettings.FILE_PROTOCOL;
import static djf.language.AppLanguageSettings.PATH_IMAGES;
import static djf.language.AppLanguageSettings.PATH_WORK;
import static met.css.metStyle.*;
import static met.metPropertyType.*;

/**
 * Created by Kitcatski on 11/9/2017.
 */
public class metWelcome extends AppWelcomeComponent{


    //Pane on left side of welcome screen that holds previous projects
    VBox recentWorkPane;
    Label recentWork;
    VBox logoPane;
    ImageView logo;


    public metWelcome(){
        getNamesFromData();
        initLayout();
        initStyle();

    }
    public void initLayout(){

        recentWorkPane = new VBox();
        recentWork = new Label("Recent Work");
        logoPane = new VBox();
        Image image = new Image(FILE_PROTOCOL + PATH_IMAGES + "Logo.png");
        logo = new ImageView(image);
        logo.setFitHeight(230);
        logo.setFitWidth(650);
        newMapLink = new Hyperlink("Create New Map");
        logoPane.getChildren().addAll(logo,newMapLink);
        logoPane.setAlignment(Pos.TOP_CENTER);
        logoPane.setSpacing(300);
        recentWorkPane.getChildren().addAll(recentWork);
        for(int i = 0; i < recentWorkLinks.size(); i++){
            recentWorkPane.getChildren().add(recentWorkLinks.get(i));
        }
        recentWorkPane.setMinWidth(300);
        recentWorkPane.setAlignment(Pos.TOP_CENTER);
        welcomePane = new BorderPane();
        welcomePane.setLeft(recentWorkPane);
        welcomePane.setCenter(logoPane);

    }
    public void initStyle(){
        String recentWorkStyle = "     -fx-padding: 0px;\n" +
                "     -fx-spacing: 10;\n" +
                "     -fx-background-color: #ffffcc;\n" +
                "     -fx-background-radius: 5.0;\n" +
                "     -fx-padding: 15;\n" +
                "     -fx-spacing: 10;\n" +
                "     -fx-border-width: 2px;\n" +
                "     -fx-border-color: #7777dd;";
        recentWorkPane.setStyle(recentWorkStyle);
        newMapLink.setStyle("-fx-font-size:2.5em;");
        System.out.println(recentWorkPane.getStyleClass());
        logoPane.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        welcomePane.getStyleClass().add(INNER_TOOLBAR_ROW);
    }
    //Used to get file names to create hyperlinks of recent work
    public void getNamesFromData(){
        recentWorkLinks = new ArrayList<>();
        recentWorkNames = new ArrayList<>();
        File folder = new File(PATH_WORK);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++){
            if(listOfFiles[i].isFile()){
                String name = listOfFiles[i].getName().substring(0,listOfFiles[i].getName().length()-3);
                recentWorkLinks.add(new Hyperlink(name));
                recentWorkNames.add(name);
            }
        }

    }
    public ArrayList<String> getRecentWorkNames(){
        return recentWorkNames;
    }
    public ArrayList<Hyperlink> getRecentWorkLinks(){
        return recentWorkLinks;
    }
}
