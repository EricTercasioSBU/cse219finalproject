package met.transactions;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;
import met.data.metData;

/**
 * Created by Kitcatski on 12/8/2017.
 */
public class RemoveNode_Transaction implements jTPS_Transaction {
    private metData data;
    private Node node;
    private int nodeIndex;

    public RemoveNode_Transaction(metData data, Node node) {
        this.data = data;
        this.node = node;
        Pane parent = (Pane) node.getParent();
        nodeIndex = parent.getChildren().indexOf(node);
    }

    @Override
    public void doTransaction() {
        data.removeNode(node);
    }

    @Override
    public void undoTransaction() {
        data.addNodeAtIndex(nodeIndex,node);


    }
}
