package met.transactions;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 * Created by Eric on 12/3/2017.
 */
public class ItalicText_Transaction implements jTPS_Transaction {
    Text text;
    Font previousFont;


    public ItalicText_Transaction(Text text){
        this.text = text;
        previousFont = text.getFont();
    }
    @Override
    public void doTransaction() {
        if(text.getFont().getStyle().equals("Bold")){ //IF TEXT IS ONLY BOLD
            text.setFont(Font.font(text.getFont().getFamily(),FontWeight.BOLD,FontPosture.ITALIC,text.getFont().getSize()));
        }else if(text.getFont().getStyle().equals("Italic")){ //IF TEXT IS ONLY ITALIC
            text.setFont(Font.font(text.getFont().getFamily(),FontPosture.REGULAR,text.getFont().getSize()));
        }else if(text.getFont().getStyle().equals("Bold Italic")){ //IF TEXT IS BOLD AND ITALIC
            text.setFont(Font.font(text.getFont().getFamily(), FontWeight.BOLD,FontPosture.REGULAR,text.getFont().getSize()));
        }else if(text.getFont().getStyle().equals("Regular")){// IF TEXT IS NOT BOLD NOR ITALIC
            text.setFont(Font.font(text.getFont().getFamily(), FontPosture.ITALIC,text.getFont().getSize()));
        }
    }

    @Override
    public void undoTransaction() {
        text.setFont(previousFont);
    }

}
