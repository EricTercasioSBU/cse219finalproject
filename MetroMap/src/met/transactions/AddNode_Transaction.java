package met.transactions;

import javafx.scene.Node;
import jtps.jTPS_Transaction;
import met.data.metData;

/**
 * Created by Eric on 11/18/2017.
 */
public class AddNode_Transaction implements jTPS_Transaction {
    private metData data;
    private Node node;

    public AddNode_Transaction(metData initData, Node initNode) {
        data = initData;
        node = initNode;
    }

    @Override
    public void doTransaction() {
        data.addNode(node);
    }

    @Override
    public void undoTransaction() {
        data.removeNode(node);
    }
}
