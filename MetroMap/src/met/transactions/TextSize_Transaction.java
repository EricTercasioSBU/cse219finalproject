package met.transactions;

/**
 * Created by Eric on 12/3/2017.
 */
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author Eric
 */
public class TextSize_Transaction implements jTPS_Transaction {
    Text text;
    int previousSize;
    int selectedSize;

    public TextSize_Transaction(Text text,  int selectedSize){
        this.text = text;
        this.selectedSize = selectedSize;
        previousSize = (int)text.getFont().getSize();
    }

    @Override
    public void doTransaction() {
        text.setFont(Font.font(text.getFont().getFamily(),selectedSize));
    }

    @Override
    public void undoTransaction() {
        text.setFont(Font.font(text.getFont().getFamily(),previousSize));

    }

}