package met.transactions;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 * Created by Eric on 12/3/2017.
 */
public class TextFont_Transaction implements jTPS_Transaction {
    Text text;
    String selectedFont;
    Font previousFont;

    public TextFont_Transaction(Text text, String selectedFont){
        this.text = text;
        this.selectedFont = selectedFont;
        previousFont = text.getFont();

    }
    @Override
    public void doTransaction() {
        if(text.getFont().getStyle().equals("Bold")){ //IF TEXT IS ONLY BOLD
            text.setFont(Font.font(selectedFont, FontWeight.BOLD,text.getFont().getSize()));
        }else if(text.getFont().getStyle().equals("Italic")){ //IF TEXT IS ONLY ITALIC
            text.setFont(Font.font(selectedFont, FontPosture.ITALIC,text.getFont().getSize()));
        }else if(text.getFont().getStyle().equals("Bold Italic")){ //IF TEXT IS BOLD AND ITALIC
            text.setFont(Font.font(selectedFont,FontWeight.BOLD,FontPosture.ITALIC,text.getFont().getSize()));
        }else if(text.getFont().getStyle().equals("Regular")){// IF TEXT IS NOT BOLD NOR ITALIC
            text.setFont(Font.font(selectedFont,text.getFont().getSize()));
        }
    }

    @Override
    public void undoTransaction() {
        text.setFont(previousFont);
    }
}

