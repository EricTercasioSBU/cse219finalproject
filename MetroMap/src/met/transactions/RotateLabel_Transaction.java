package met.transactions;

import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 * Created by Eric on 12/8/2017.
 */
public class RotateLabel_Transaction implements jTPS_Transaction {
    Text text;

    public RotateLabel_Transaction(Text text) {
        this.text = text;
    }

    @Override
    public void doTransaction() {
        if (text.getRotate() == 90) {
            text.setRotate(0);
        } else {
            text.setRotate(90);
        }

    }

    @Override
    public void undoTransaction() {
        if (text.getRotate() == 90) {
            text.setRotate(0);
        } else {
            text.setRotate(90);
        }

    }
}
