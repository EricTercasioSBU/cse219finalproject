package met.transactions;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;
import met.data.DraggableStation;
import met.data.DraggableText;

/**
 * Created by Kitcatski on 12/8/2017.
 */
public class MoveLabel_Transaction implements jTPS_Transaction {
    Text text;
    DraggableStation station;
    double previousX;
    double previousY;
    int stationX;
    int stationY;
    double textX;
    double textY;
    DoubleBinding topRightXProperty;
    DoubleBinding topRightYProperty;
    DoubleBinding bottomRightXProperty;
    DoubleBinding bottomRightYProperty;
    DoubleBinding bottomLeftXProperty;
    DoubleBinding bottomLeftYProperty;
    DoubleBinding topLeftXProperty;
    DoubleBinding topLeftYProperty;
    DoubleBinding previousXBinding;
    DoubleBinding previousYBinding;

    public MoveLabel_Transaction(DraggableStation station, DraggableText text){
        this.text = text;
        this.station = station;
        textX = text.getX();
        textY = text.getY();
        stationX = (int) station.getCenterX();
        stationY = (int) station.getCenterY();
        //Set top right binding
        topRightXProperty = station.centerXProperty().add(20);
        topRightYProperty = station.centerYProperty().subtract(20);
        //Bottom Right
        bottomRightXProperty = station.centerXProperty().add(20);
        bottomRightYProperty = station.centerYProperty().add(20);
        //Bottom Left
        bottomLeftXProperty = station.centerXProperty().subtract(20);
        bottomLeftYProperty = station.centerYProperty().add(20);
        //Top left
        topLeftXProperty = station.centerXProperty().subtract(20);
        topLeftYProperty = station.centerYProperty().subtract(20);


    }

    @Override
    public void doTransaction() {
        //Top Right
        if((textX == stationX + 20) && (textY == stationY - 20)){
            //Move to bottom right
            previousXBinding = topRightXProperty;
            previousYBinding = topRightYProperty;
            text.xProperty().unbind();
            text.yProperty().unbind();
            text.xProperty().bind(bottomRightXProperty);
            text.yProperty().bind(bottomRightYProperty);

        }
        //Bottom Right
        if((textX == stationX + 20) && (textY == stationY + 20)){
            //Move to bottom left
            previousXBinding = bottomRightXProperty;
            previousYBinding = bottomRightYProperty;
            text.xProperty().unbind();
            text.yProperty().unbind();
            text.xProperty().bind(bottomLeftXProperty);
            text.yProperty().bind(bottomLeftYProperty);
        }
        //Bottom Left
        if((textX == stationX - 20) && (textY == stationY + 20)){
            previousXBinding = bottomLeftXProperty;
            previousYBinding = bottomLeftYProperty;
            text.xProperty().unbind();
            text.yProperty().unbind();
            text.xProperty().bind(topLeftXProperty);
            text.yProperty().bind(topLeftYProperty);

        }
        //Top Left
        if((textX == stationX - 20) && (textY == stationY - 20)){
            previousXBinding = topLeftXProperty;
            previousYBinding = topLeftYProperty;
            text.xProperty().unbind();
            text.yProperty().unbind();
            text.xProperty().bind(topRightXProperty);
            text.yProperty().bind(topRightYProperty);

        }

    }

    @Override
    public void undoTransaction() {
        text.xProperty().unbind();
        text.yProperty().unbind();
        text.xProperty().bind(previousXBinding);
        text.yProperty().bind(previousYBinding);
    }
}
