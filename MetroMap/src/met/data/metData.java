package met.data;


import djf.AppTemplate;
import djf.components.AppDataComponent;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import jtps.jTPS;
import met.gui.AddLineDialog;
import met.gui.metWorkspace;
import met.transactions.AddNode_Transaction;
import met.transactions.MoveLabel_Transaction;
import met.transactions.RemoveNode_Transaction;
import met.transactions.RotateLabel_Transaction;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

import static met.data.metState.SELECTING_NODE;
import static met.data.metState.SIZING_LINE_END;

/**
 * Created by Kitcatski on 11/3/2017.
 */
public class metData implements AppDataComponent {
    ObservableList<Node> mapNodes;

    AppTemplate app;

    Node newShape;

    Node selectedNode;

    DropShadow highlightedEffect;

    Color backgroundColor;

    ArrayList<String> lineNames;

    ArrayList<String> stationNames;

    //Current state of the program
    metState state;

    String changedLineName;

    Color changedLineColor;

    HashMap intersections;
    String backgroundImagePath;

    public metData(AppTemplate initApp) {
        // KEEP THE APP FOR LATER
        app = initApp;

        // NO SHAPE STARTS OUT AS SELECTED
        newShape = null;
        selectedNode = null;

        // THIS IS FOR THE SELECTED SHAPE
        DropShadow dropShadowEffect = new DropShadow();
        dropShadowEffect.setOffsetX(0.0f);
        dropShadowEffect.setOffsetY(0.0f);
        dropShadowEffect.setSpread(1.0);
        dropShadowEffect.setColor(Color.YELLOW);
        dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
        dropShadowEffect.setRadius(15);
        highlightedEffect = dropShadowEffect;

        //Arraylists for the comboboxs
        lineNames = new ArrayList<>();
        stationNames = new ArrayList<>();
    }

    @Override
    public void resetData() {
        setState(SELECTING_NODE);
        newShape = null;
        selectedNode = null;
        BackgroundFill backgroundFill = new BackgroundFill(Color.TRANSPARENT, null, null);
        Background background = new Background(backgroundFill);
        ((metWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().clear();
        ((metWorkspace) app.getWorkspaceComponent()).getCanvas().setBackground(background);

    }

    public Color getBackgroundColor() {
        if(!((metWorkspace)app.getWorkspaceComponent()).getCanvas().getBackground().getFills().isEmpty())
        return (Color) ((metWorkspace) app.getWorkspaceComponent()).getCanvas().getBackground().getFills().get(0).getFill();
        return null;
    }

    public void setBackgroundColor(Color color) {
        Pane canvas = ((metWorkspace) app.getWorkspaceComponent()).getCanvas();
        BackgroundFill fill = new BackgroundFill(color, null, null);
        Background background = new Background(fill);
        canvas.setBackground(background);
    }
    public void setBackgroundImage(BackgroundImage image){
        Pane canvas = ((metWorkspace)app.getWorkspaceComponent()).getCanvas();
        canvas.setBackground(new Background(image));
    }

    public void startNewLabel(String labelText, int x, int y) {
        DraggableText text = new DraggableText(labelText);
        text.setX(x);
        text.setY(y);
        AddNode_Transaction ant = new AddNode_Transaction(this,text);
        app.getTPS().addTransaction(ant);
    }

    public void removeLine(String lineToDelete) {
        ArrayList<Node> nodes = new ArrayList<>();
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup && ((DraggableGroup) mapNodes.get(i)).getType().equals("Line")) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                if (((DraggableText) group.getChildren().get(0)).getText().equals(lineToDelete)) {
                    //Remove from comboBox
                    lineNames.remove(lineToDelete);
                    for (int j = 0; j < group.getChildren().size(); j++) {
                        if (group.getChildren().get(j) instanceof DraggableGroup) {
                            nodes.add(group.getChildren().get(j));
                        }
                    }
                    jTPS tps = app.getTPS();
                    metData data = (metData) app.getDataComponent();
                    RemoveNode_Transaction newTransaction = new RemoveNode_Transaction(data, newShape);
                    tps.addTransaction(newTransaction);

                    for (int k = 0; k < nodes.size(); k++) {
                        mapNodes.add(i, nodes.get(k));
                        i++;
                    }
                    break;
                }
            }
        }

    }


    public void startNewLine(String lineName, Color color, int x, int y) {
        //For quad line DraggableLine draggableLine = new DraggableLine(x-50,y,x,y,x+50,y);
        DraggableLine draggableLine = new DraggableLine(x - 50, y, x + 50, y);
        DraggableText leftSideName = new DraggableText(lineName);
        DraggableText rightSideName = new DraggableText(lineName);
//        int leftSideX = (int) (draggableLine.getStartX() - (int)leftSideName.getWidth() - 10); //For quad curve
//        int rightSideX = (int) (draggableLine.getEndX() + 10);
        draggableLine.setStroke(color);
        int leftSideX = (int) (draggableLine.getStartX() - (int) leftSideName.getWidth() - 10);
        int rightSideX = (int) (draggableLine.getEndX() + 10);
        leftSideName.start(leftSideX, (y));
        rightSideName.start(rightSideX, (y));
        DraggableGroup draggableGroup = new DraggableGroup("Line");
        draggableGroup.getChildren().addAll(leftSideName, draggableLine, rightSideName);
        draggableGroup.start(x, y);
        draggableGroup.setLineName(lineName);
        newShape = draggableGroup;
        lineNames.add(draggableGroup.getLineName());
        double width = leftSideName.getWidth();
        draggableLine.startXProperty().bind(leftSideName.xProperty().add(width + 10));
        draggableLine.startYProperty().bind(leftSideName.yProperty());
        draggableLine.endXProperty().bind(rightSideName.xProperty().subtract(width - 20));
        draggableLine.endYProperty().bind(rightSideName.yProperty());
        initNewShape();

    }

    public void editLine(String lineName) {
        String previousName = null;
        Color previousColor = null;
        //This value used so we dont have to search for it again
        Group groupToChange = null;

        ArrayList<DraggableText> textsToChange = new ArrayList<>();
        ArrayList<DraggableLine> linesToChange = new ArrayList<>();
        //getting previous
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup && ((DraggableGroup) mapNodes.get(i)).getType().equals("Line")) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                for (int j = 0; j < group.getChildren().size(); j++) {
                    if (group.getChildren().get(j) instanceof DraggableText) {
                        if (((DraggableText) group.getChildren().get(j)).getText().equals(lineName)) {
                            groupToChange = (DraggableGroup) mapNodes.get(i);
                            previousColor = (Color) ((DraggableLine) group.getChildren().get(j + 1)).getStroke();
                            previousName = ((DraggableText) group.getChildren().get(j)).getText();
                            break;

                        }
                    }
                }
            }
        }
        AddLineDialog dialog = new AddLineDialog();
        dialog.setDialogContextText("Edit Line");
        dialog.setCurrentText(previousName);
        dialog.setCurrentColor(previousColor);

        for (int i = 0; i < groupToChange.getChildren().size(); i++) {
            if (groupToChange.getChildren().get(i) instanceof DraggableText) {
                textsToChange.add((DraggableText) groupToChange.getChildren().get(i));
            }
            if (groupToChange.getChildren().get(i) instanceof DraggableLine) {
                linesToChange.add((DraggableLine) groupToChange.getChildren().get(i));
            }
        }
        dialog.getOkayButton().setOnAction(e -> {

            String newName = dialog.getNameResult();
            Color newColor = dialog.getColorResult();
            //First change the text
            for (int i = 0; i < textsToChange.size(); i++) {
                textsToChange.get(i).setText(newName);
            }
            for (int i = 0; i < linesToChange.size(); i++) {
                linesToChange.get(i).setStroke(newColor);
            }
            lineNames.remove(lineName);
            lineNames.add(newName);

            dialog.close();
            //because setonaction
            app.getWorkspaceComponent().reloadWorkspace(this);

        });


    }

    //Only using this method for when we load in a map, to reset all the bindings since were not saving them
    public void bindAllNodes() {
        //Go through all groups
        for (int i = 0; i < mapNodes.size(); i++) {
            //Lines left side always binded to node left of it, and right side always binded to node to right of it
            //So look for lines
            //But first, only thing in mapNodes is groups, so look for groups
            if (mapNodes.get(i) instanceof DraggableGroup) {
                DraggableGroup mapGroup = (DraggableGroup) mapNodes.get(i);
                for (int k = 0; k < mapGroup.getChildren().size(); k++) {
                    //Its only a linegroup if type equals line
                    if (mapGroup.getType().equals("Line")) {
                        if (mapGroup.getChildren().get(k) instanceof DraggableLine) {
                            //Only combinations that could be next to it are Text And DraggableGroup
                            // (which would have station in it that we would bind to)
                            //So first check the left side
                            DraggableLine line = (DraggableLine) mapGroup.getChildren().get(k);
                            if (mapGroup.getChildren().get(k - 1) instanceof DraggableText) {
                                DraggableText text = (DraggableText) mapGroup.getChildren().get(k - 1);
                                double width = text.getWidth();
                                line.startXProperty().bind(text.xProperty().add(width + 10));
                                line.startYProperty().bind(text.yProperty());
                            } else if (mapGroup.getChildren().get(k - 1) instanceof DraggableGroup) {
                                DraggableGroup group = (DraggableGroup) mapGroup.getChildren().get(k - 1);
                                DraggableStation station = (DraggableStation) group.getChildren().get(0);
                                line.startXProperty().bind(station.centerXProperty());
                                line.startYProperty().bind(station.centerYProperty());
                            }
                            //Now we check the right side
                            if (mapGroup.getChildren().get(k + 1) instanceof DraggableText) {
                                DraggableText text = (DraggableText) mapGroup.getChildren().get(k + 1);
                                double width = text.getWidth();
                                line.endXProperty().bind(text.xProperty().subtract(width - 10));
                                line.endYProperty().bind(text.yProperty());
                            } else if (mapGroup.getChildren().get(k + 1) instanceof DraggableGroup) {
                                DraggableGroup group = (DraggableGroup) mapGroup.getChildren().get(k + 1);
                                DraggableStation station = (DraggableStation) group.getChildren().get(0);
                                line.endXProperty().bind(station.centerXProperty());
                                line.endYProperty().bind(station.centerYProperty());
                                DraggableText text = (DraggableText) group.getChildren().get(1);
                                text.xProperty().bind(station.centerXProperty().add(station.getWidth()));
                                text.yProperty().bind(station.centerYProperty().subtract(station.getHeight()));
                            }
                        }
                    }
                    //Only other possibility that we're looking for is a standalone station
                    if (mapGroup.getType().equals("Station")) {
                        DraggableStation station = (DraggableStation) mapGroup.getChildren().get(0);
                        DraggableText text = (DraggableText) mapGroup.getChildren().get(1);
                        text.xProperty().bind(station.centerXProperty().add(station.getWidth()));
                        text.yProperty().bind(station.centerYProperty().subtract(station.getHeight()));

                    }
                }
            }
        }
    }

    //Used in order to load back into both the line and station
    //combo box's after loading a file
    public void loadIntoComboBoxes() {
        lineNames.clear();
        stationNames.clear();
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup) {
                if (((DraggableGroup) mapNodes.get(i)).getType().equals("Line")) {
                    DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                    DraggableText text = (DraggableText) group.getChildren().get(0);
                    lineNames.add(text.getText());
                    for (int k = 0; k < group.getChildren().size(); k++) {
                        if (group.getChildren().get(k) instanceof DraggableGroup) {
                            DraggableGroup innerGroup = (DraggableGroup) group.getChildren().get(k);
                            DraggableText innerText = (DraggableText) innerGroup.getChildren().get(1);
                            stationNames.add(innerText.getText());
                        }
                    }

                } else if (((DraggableGroup) mapNodes.get(i)).getType().equals("Station")) {
                    DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                    DraggableText text = (DraggableText) group.getChildren().get(1);
                    stationNames.add(text.getText());

                }
            }
        }
        app.getWorkspaceComponent().reloadWorkspace(this);
    }

    public void startNewStation(String lineName, int x, int y) {
        DraggableStation draggableStation = new DraggableStation(x, y);
        DraggableText stationName = new DraggableText(lineName);
        int textX = (int) (draggableStation.getCenterX() + draggableStation.getWidth());
        int textY = (int) (draggableStation.getCenterY() - draggableStation.getHeight());
        stationName.start(textX, textY);
        DraggableGroup draggableGroup = new DraggableGroup("Station");
        draggableGroup.getChildren().addAll(draggableStation, stationName);
        draggableGroup.start(x, y);
        newShape = draggableGroup;
        stationName.xProperty().bind(draggableStation.centerXProperty().add(draggableStation.getWidth()));
        stationName.yProperty().bind(draggableStation.centerYProperty().subtract(draggableStation.getHeight()));
        stationNames.add(lineName);
        initNewShape();

    }

    public void removeStation(String stationName) {
        Color lineColor = null;
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                if (group.getChildren().get(0) instanceof DraggableStation &&
                        ((DraggableText) group.getChildren().get(1)).getText().equals(stationName)) {
                    mapNodes.remove(mapNodes.get(i));
                } else if (group.getChildren().get(0) instanceof DraggableText) {
                    DraggableGroup lineGroup = null;
                    lineColor = (Color) ((DraggableLine) group.getChildren().get(1)).getStroke();
                    for (int k = 0; k < group.getChildren().size(); k++) {
                        if (group.getChildren().get(k) instanceof DraggableGroup &&
                                ((DraggableGroup) group.getChildren().get(k)).getChildren().get(0) instanceof DraggableStation) {
                            if (((DraggableText) ((DraggableGroup) group.getChildren().get(k))
                                    .getChildren().get(1)).getText().equals(stationName)) {
                                //This means we found it
                                //Delete all lines and selected station, then make new lines and re bind
                                group.getChildren().remove(k);
                                lineGroup = group;
                            }
                        }
                    }
                    if (lineGroup != null) {
                        //This means we reconstruct the line
                        //First go through the line
                        for (int z = 0; z < lineGroup.getChildren().size(); z++) {
                            //If its a line, delete
                            if (lineGroup.getChildren().get(z) instanceof DraggableLine) {
                                lineGroup.getChildren().remove(z);
                                z = z - 1;

                            }
                        }
                        //Now rebuild the line
                        rebuildLine(lineGroup, lineColor);


                    }

                }
            }
        }
        stationNames.remove(stationName);
    }


    public void initNewShape() {
        // DESELECT THE SELECTED SHAPE IF THERE IS ONE
//        if (selectedNode != null) {
//            unhighlightNode(selectedNode);
//            selectedNode = null;
//        }

        // USE THE CURRENT SETTINGS FOR THIS NEW SHAPE
        metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
//        newShape.setFill(workspace.getFillColorPicker().getValue());
//        newShape.setStroke(workspace.getOutlineColorPicker().getValue());
//        newShape.setStrokeWidth(workspace.getOutlineThicknessSlider().getValue());

        // GO INTO SHAPE SIZING MODE
        // state = metState.SIZING_SHAPE;

        // FINALLY, ADD A TRANSACTION FOR ADDING THE NEW SHAPE
        jTPS tps = app.getTPS();
        metData data = (metData) app.getDataComponent();
        AddNode_Transaction newTransaction = new AddNode_Transaction(data, newShape);
        tps.addTransaction(newTransaction);
        workspace.reloadWorkspace(data);
    }

    public void removeNode(Node nodeToRemove) {
        int currentIndex = mapNodes.indexOf(nodeToRemove);
        if (currentIndex >= 0) {
            mapNodes.remove(currentIndex);
        }
    }

    public void addNode(Node nodeToAdd) {
        int currentIndex = mapNodes.indexOf(nodeToAdd);
        if (currentIndex < 0) {
            mapNodes.add(nodeToAdd);
        }
    }

    public Node selectTopNode(int x, int y) {
        Node node = getTopNode(x, y);
        if (node == selectedNode)
            return node;

        if (selectedNode != null) {
            unhighlightNode(selectedNode);
        }
        if (node != null) {
            if (node instanceof DraggableGroup) {
                DraggableGroup group = (DraggableGroup) node;
                for (int i = 0; i < group.getChildren().size(); i++) {
                    if (group.getChildren().get(i).contains(x, y)) {
                        if (group.getChildren().get(i) instanceof DraggableText && group.getType().equals("Line")) {
                            highlightNode(group.getChildren().get(i));
                            setState(SIZING_LINE_END);
                            node = group.getChildren().get(i);
                            break;
                        } else if (group.getChildren().get(i) instanceof DraggableStation) {
                            highlightNode(group.getChildren().get(i));
                            setState(SELECTING_NODE);
                            node = group.getChildren().get(i);
                            break;

                        } else if (group.getChildren().get(i) instanceof DraggableGroup) {
                            Group innerGroup = (Group) group.getChildren().get(i);
                            for (int j = 0; j < innerGroup.getChildren().size(); j++) {
                                if (innerGroup.getChildren().get(j) instanceof DraggableStation)
                                    setState(SELECTING_NODE);
                                node = innerGroup.getChildren().get(j);
                                break;
                            }
                        }
                    }
                }
            } else {
                setState(SELECTING_NODE);
                highlightNode(node);
            }
            metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
            //workspace.loadSelectedNodeSettings(node);
        }
        selectedNode = node;
        if (node != null) {
            ((Draggable) node).setStart(x, y);
        }

        return node;
    }

    public Node getTopNode(int x, int y) {
        for (int i = mapNodes.size() - 1; i >= 0; i--) {
            Node node = (Node) mapNodes.get(i);
            if (node.contains(x, y)) {
                if (node instanceof DraggableGroup && ((DraggableGroup) node).getType().equals("Line")) {
                    for (int k = 0; k < ((DraggableGroup) node).getChildren().size(); k++) {
                        if (((DraggableGroup) node).getChildren()
                                .get(k) instanceof DraggableGroup) {
                            DraggableGroup innerGroup = (DraggableGroup) ((DraggableGroup) node).getChildren().get(k);
                            if (innerGroup.contains(x, y)) {
                                return innerGroup;
                            }

                        }
                    }
                }
                if (node instanceof Draggable) {
                    return node;
                }
            }
        }
        return null;
    }

    public void addStationToLine(String stationToAddLineTo, Node node) {
        DraggableGroup groupToAddTo = null;
        Color lineColor = null;
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup && ((DraggableGroup) mapNodes.get(i)).getType().equals("Line")) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                for (int j = 0; j < group.getChildren().size(); j++) {
                    if (group.getChildren().get(j) instanceof DraggableText) {
                        if (((DraggableText) group.getChildren().get(j)).getText().equals(stationToAddLineTo)) {
                            groupToAddTo = (DraggableGroup) mapNodes.get(i);
                            lineColor = (Color) ((DraggableLine) group.getChildren().get(j + 1)).getStroke();
                            break;

                        }
                    }
                }
            }
        }
        //Create new line
        DraggableLine newLine = new DraggableLine(0, 0, 0, 0);
        newLine.setStroke(lineColor);
        //Move new line and the station next to the text
        groupToAddTo.getChildren().add(1, newLine);
        groupToAddTo.getChildren().add(2, node);
        DraggableLine line = (DraggableLine) groupToAddTo.getChildren().get(3);
        DraggableStation station = (DraggableStation) ((DraggableGroup) node).getChildren().get(0);
//        DraggableText text = (DraggableText) ((DraggableGroup)node).getChildren().get(1);
//        station.setCenterX(line.getControlX()); for quad curve
//        station.setCenterY(line.getControlY());
        //Place location of station in middle length of original line
        station.setCenterX((line.getEndX() + line.getStartX()) / 2);
        station.setCenterY((line.getEndY() + line.getStartY()) / 2);
        //Save inital start of original line
        double startX = line.getStartX();
        double startY = line.getStartY();
        //First unbind the original line
        line.startXProperty().unbind();
        line.startYProperty().unbind();
        //Cut length of original line in half

        line.setStartX((line.getEndX() + line.getStartX()) / 2);
        line.setStartY((line.getEndY() + line.getStartY()) / 2);
        //Set new line to length of other half
        newLine.setStartX(startX);
        newLine.setStartY(startY);
        //Match it up to the new original
        newLine.setEndX(line.getStartX());
        newLine.setEndY(line.getStartY());
        //Move the locations for the front text
        DraggableText textToMove = (DraggableText) groupToAddTo.getChildren().get(0);
        textToMove.setX(newLine.getStartX() - textToMove.getWidth() - 10);
        //Set bindings for new line
        double width = textToMove.getWidth();
        //First start with front

        newLine.startXProperty().bind(textToMove.xProperty().add(width + 10));
        newLine.startYProperty().bind(textToMove.yProperty());
        //Then end of line
        newLine.endXProperty().bind(station.centerXProperty());
        newLine.endYProperty().bind(station.centerYProperty());

        line.startXProperty().bind(station.centerXProperty());
        line.startYProperty().bind(station.centerYProperty());
        removeNode(node);
    }

    public void removeStationFromLine(String lineSelection, DraggableGroup stationSelection) {

        DraggableGroup lineGroup = null;
        Color lineColor = null;
        //Find it
        for (int i = 0; i < mapNodes.size(); i++) {
            DraggableGroup group = (DraggableGroup) mapNodes.get(i);
            if (group.getChildren().get(0) instanceof DraggableText && ((DraggableText) group.getChildren().get(0)).getText().equals(lineSelection)) {
                for (int k = 0; k < group.getChildren().size(); k++) {
                    if (group.getChildren().get(k).equals(stationSelection)) {
                        lineGroup = group;
                        lineColor = (Color) ((DraggableLine) group.getChildren().get(k + 1)).getStroke();
                        group.getChildren().remove(k);
                    }
                }
            }


        }
        if (lineGroup != null) {
            //This means we reconstruct the line
            //First go through the line
            for (int z = 0; z < lineGroup.getChildren().size(); z++) {
                //If its a line, delete
                if (lineGroup.getChildren().get(z) instanceof DraggableLine) {
                    lineGroup.getChildren().remove(z);
                    z = z - 1;

                }
            }
            rebuildLine(lineGroup, lineColor);

        }
        //Not deleting station, so lets move it
        mapNodes.add(stationSelection);
        DraggableStation station = (DraggableStation) stationSelection.getChildren().get(0);
        //Just move it down a little to show its no longer on line
        station.setCenterY(station.getCenterY() + 10);
    }

    public void rebuildLine(DraggableGroup lineGroup, Color lineColor) {
        //Now go through to create new lines
        for (int j = 0; j < lineGroup.getChildren().size(); j++) {
            //make sure were not at the end
            if (lineGroup.getChildren().get(j + 1) != null) {
                DraggableLine line = new DraggableLine(0, 0, 0, 0);
                line.setStroke(lineColor);
                //If were at the beggining
                if (lineGroup.getChildren().get(j) instanceof DraggableText) {
                    line.startXProperty().bind(((DraggableText) lineGroup.getChildren().get(j))
                            .xProperty().add(((DraggableText) lineGroup.getChildren().get(j)).getWidth()));
                    line.startYProperty().bind((((DraggableText) lineGroup.getChildren().get(j))).yProperty());
                } else if (lineGroup.getChildren().get(j) instanceof DraggableGroup) {
                    DraggableStation station = (DraggableStation) ((DraggableGroup) lineGroup.getChildren().get(j))
                            .getChildren().get(0);
                    line.startXProperty().bind(station.centerXProperty());
                    line.startYProperty().bind(station.centerYProperty());
                }
                if (lineGroup.getChildren().get(j + 1) instanceof DraggableGroup) {
                    DraggableStation station = (DraggableStation) ((DraggableGroup)
                            lineGroup.getChildren().get(j + 1)).getChildren().get(0);
                    line.endXProperty().bind(station.centerXProperty());
                    line.endYProperty().bind(station.centerYProperty());
                    lineGroup.getChildren().add(j + 1, line);
                    j = j + 1;
                } else {
                    line.endXProperty().bind(((DraggableText) lineGroup.getChildren().get(j + 1)).xProperty());
                    line.endYProperty().bind(((DraggableText) lineGroup.getChildren().get(j + 1)).yProperty());
                    lineGroup.getChildren().add(j + 1, line);


                    break;
                }


            } else {
                DraggableText text = (DraggableText) lineGroup.getChildren().get(j);
                DraggableLine draggableLine = new DraggableLine(0, 0, 0, 0);
                draggableLine.setStroke(lineColor);
                DraggableStation station = (DraggableStation) ((DraggableGroup)
                        lineGroup.getChildren().get(j - 1)).getChildren().get(0);
                draggableLine.startXProperty().bind(station.centerXProperty());
                draggableLine.startYProperty().bind(station.centerYProperty());
                draggableLine.endXProperty().bind(text.xProperty());
                draggableLine.endYProperty().bind(text.yProperty());
                lineGroup.getChildren().add(j - 1, draggableLine);
            }
        }
    }

    public void createIntersection() {
        Group gridGroup = ((metWorkspace) app.getWorkspaceComponent()).getGridGroup();

    }

    public void unhighlightNode(Node node) {
        node.setEffect(null);
    }

    public void highlightNode(Node node) {
        node.setEffect(highlightedEffect);
    }


    public ObservableList<Node> getMapNodes() {
        return mapNodes;
    }

    public void setMapNodes(ObservableList<Node> mapNodes) {
        this.mapNodes = mapNodes;
    }

    public metState getState() {
        return state;
    }

    public void setState(metState initState) {
        state = initState;
    }

    public boolean isInState(metState testState) {
        return state == testState;
    }

    public Node getSelectedNode() {
        return selectedNode;
    }

    public ArrayList<String> getLineNames() {
        return lineNames;
    }

    public ArrayList<String> getStationNames() {
        return stationNames;
    }

    public String getChangedLineName() {
        return changedLineName;
    }

    public Color getChangedLineColor() {
        return changedLineColor;
    }

    public void setLineNames(ArrayList<String> lineNames) {
        this.lineNames = lineNames;
    }

    public Pane getCanvas() {
        metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        return canvas;
    }


    public void snapToGrid(Node node) {
        ArrayList<Point> intersections = ((metWorkspace) app.getWorkspaceComponent()).getGridIntersections();
        //First find the node we're looking to snap
        if (node instanceof Circle && node.getParent() instanceof DraggableGroup &&
                ((DraggableGroup) node.getParent()).getType().equals("Station")) {
            Circle station = (Circle) node;
            //Now we have to find the closest intersection
            int stationX = (int) station.getCenterX();
            int stationY = (int) station.getCenterY();
            Point startPoint = new Point(stationX, stationY);
            Point nearest = intersections.get(0);
            int previousDistance = (int) (((Math.abs(nearest.getX() - stationX)) + (Math.abs(nearest.getY() - stationY))) / 2);
            for (int i = 0; i < intersections.size(); i++) {
                Point point = intersections.get(i);
                int newDistance = (int) (((Math.abs(point.getX() - stationX)) + (Math.abs(point.getY() - stationY))) / 2);
                if (newDistance <= previousDistance) {
                    previousDistance = newDistance;
                    nearest = intersections.get(i);
                }

            }
            station.setCenterX(nearest.getX());
            station.setCenterY(nearest.getY());
        } else if (node instanceof DraggableText) {
            DraggableText text = (DraggableText) node;
            //Now we have to find the closest intersection
            int textX = (int) text.getX();
            int textY = (int) text.getY();
            Point startPoint = new Point(textX, textY);
            Point nearest = intersections.get(0);
            int previousDistance = (int) (((Math.abs(nearest.getX() - textX)) + (Math.abs(nearest.getY() - textY))) / 2);
            for (int i = 0; i < intersections.size(); i++) {
                Point point = intersections.get(i);
                int newDistance = (int) (((Math.abs(point.getX() - textX)) + (Math.abs(point.getY() - textY))) / 2);
                if (newDistance <= previousDistance) {
                    previousDistance = newDistance;
                    nearest = intersections.get(i);
                }

            }
            text.setX(nearest.getX());
            text.setY(nearest.getY());
        }
    }

    public void rotateStation(String selection) {
        DraggableGroup stationGroup = null;
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                if (group.getChildren().get(0) instanceof DraggableStation &&
                        ((DraggableText) group.getChildren().get(1)).getText().equals(selection)) {
                    //This means we found it
                    stationGroup = group;
                } else if (group.getChildren().get(0) instanceof DraggableText) {
                    for (int k = 0; k < group.getChildren().size(); k++) {
                        if (group.getChildren().get(k) instanceof DraggableGroup &&
                                ((DraggableGroup) group.getChildren().get(k)).getChildren().get(0) instanceof DraggableStation) {
                            if (((DraggableText) ((DraggableGroup) group.getChildren().get(k))
                                    .getChildren().get(1)).getText().equals(selection)) {
                                //This means we found it
                                stationGroup = (DraggableGroup) group.getChildren().get(k);
                            }
                        }
                    }
                }
            }
        }
        if (stationGroup != null) {
            Text text = (Text) stationGroup.getChildren().get(1);
            RotateLabel_Transaction rlt = new RotateLabel_Transaction(text);
            app.getTPS().addTransaction(rlt);
        }
    }

    public void changeLineThickness(double value, String selection) {
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup && ((DraggableGroup) mapNodes.get(i)).getType().equals("Line")) {
                Text lineName = (Text) ((DraggableGroup) mapNodes.get(i)).getChildren().get(0);
                String name = lineName.getText();
                if (name.equals(selection)) {
                    for (int k = 0; k < ((DraggableGroup) mapNodes.get(i)).getChildren().size(); k++) {
                        if (((DraggableGroup) mapNodes.get(i)).getChildren().get(k) instanceof DraggableLine) {
                            DraggableLine line = (DraggableLine) ((DraggableGroup) mapNodes.get(i)).getChildren().get(k);
                            line.setStrokeWidth(value);
                        }
                    }
                }
            }
        }
    }

    public void recreateGrid() {
        Boolean visible = false;
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof GridGroup) {
                GridGroup group = (GridGroup) mapNodes.get(i);
                if (group.isVisible()) {
                    visible = true;
                }
                mapNodes.remove(mapNodes.get(i));
                break;
            }
        }
        metWorkspace workspace = (metWorkspace) app.getWorkspaceComponent();
        workspace.createGrid();
        workspace.getGridGroup().setVisible(visible);
    }

    public void changeStationRadius(double value, String selection) {
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup && ((DraggableGroup) mapNodes.get(i)).getType().equals("Station")
                    && ((DraggableText) ((DraggableGroup) mapNodes.get(i)).getChildren().get(1)).getText().equals(selection)) {
                DraggableStation station = (DraggableStation) ((DraggableGroup) mapNodes.get(i)).getChildren().get(0);
                station.setRadius(value);
            } else if (mapNodes.get(i) instanceof DraggableGroup && (((DraggableGroup) mapNodes.get(i)).getType().equals("Line"))) {
                //See if the station is in the line
                DraggableGroup lineGroup = (DraggableGroup) mapNodes.get(i);
                for (int k = 0; k < lineGroup.getChildren().size(); k++) {
                    if (lineGroup.getChildren().get(k) instanceof DraggableGroup) {
                        DraggableGroup innerGroup = (DraggableGroup) lineGroup.getChildren().get(k);
                        DraggableText text = (DraggableText) innerGroup.getChildren().get(1);
                        if (text.getText().equals(selection)) {
                            DraggableStation station = (DraggableStation) innerGroup.getChildren().get(0);
                            station.setRadius(value);

                        }
                    }

                }
            }
        }
    }

    public DraggableGroup findStationByName(String selection) {
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup && (((DraggableGroup) mapNodes.get(i)).getType().equals("Line"))) {
                //See if the station is in the line
                DraggableGroup lineGroup = (DraggableGroup) mapNodes.get(i);
                for (int k = 0; k < lineGroup.getChildren().size(); k++) {
                    if (lineGroup.getChildren().get(k) instanceof DraggableGroup) {
                        DraggableGroup innerGroup = (DraggableGroup) lineGroup.getChildren().get(k);
                        DraggableText text = (DraggableText) innerGroup.getChildren().get(1);
                        if (text.getText().equals(selection)) {
                            return innerGroup;

                        }
                    }

                }
            }
        }
        return null;
    }

    public ArrayList<DraggableGroup> getStationsInLine(DraggableGroup startLineGroup) {
        ArrayList<DraggableGroup> stations = new ArrayList<>();
        for (int i = 0; i < startLineGroup.getChildren().size(); i++) {
            if (startLineGroup.getChildren().get(i) instanceof DraggableGroup)
                stations.add((DraggableGroup) startLineGroup.getChildren().get(i));
        }
        return stations;
    }

    public void findConnections() {
        //Make an arrayList of all the stations
        ArrayList<DraggableGroup> stations = getStations();
        ArrayList<Double> xPoints = new ArrayList<>();
        ArrayList<Double> yPoints = new ArrayList<>();
        for (int i = 0; i < stations.size(); i++) {
            boolean found = false;
            DraggableGroup station = stations.get(i);
            DraggableStation innerStation = (DraggableStation) station.getChildren().get(0);
            int x = (int) innerStation.getCenterX();
            int y = (int) innerStation.getCenterY();
            xPoints.clear();
            yPoints.clear();
            xPoints.add(innerStation.getCenterX());
            yPoints.add(innerStation.getCenterY());
            for (int z = 1; z < innerStation.getRadius(); z++) {
                double xLeftBound = x - z;
                double xRightBound = x + z;
                double yUpperBound = y + z;
                double yLowerBound = y - z;
                xPoints.add(xLeftBound);
                xPoints.add(xRightBound);
                yPoints.add(yUpperBound);
                yPoints.add(yLowerBound);
            }
            for (int k = 0; k < mapNodes.size(); k++) {
                Node node = mapNodes.get(k);
                if (node instanceof DraggableGroup && ((DraggableGroup) node).getType().equals("Line")) {
                    if (node != station.getParent()) {
                        for (int h = 0; h < xPoints.size(); h++) {
                            if (node.contains(xPoints.get(h), yPoints.get(h))) {
                                station.setConnection((DraggableGroup) node, xPoints.get(h), yPoints.get(h));
                                found = true;

                            }

                        }

                    }
                }
            }
            if (!found) {
                station.setConnection(null, 0, 0);
            }
        }//Printing for checking
//        for(int i = 0; i < stations.size(); i++){
//            DraggableGroup station = stations.get(i);
//            if(station.getConnection() != null){
//                Text text = (Text) station.getChildren().get(1);
//                DraggableText text2 = (DraggableText) station.getConnection().getChildren().get(0);
//                System.out.println("Station Name : "+text.getText() + " Connection : " +text2.getText());
//            }
//        }

    }

    public ArrayList<DraggableGroup> getStations() {
        ArrayList<DraggableGroup> stations = new ArrayList<>();
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                if (((DraggableGroup) mapNodes.get(i)).getType().equals("Station")) {
                    stations.add(group);
                } else if (group.getType().equals("Line")) {
                    for (int k = 0; k < group.getChildren().size(); k++) {
                        if (group.getChildren().get(k) instanceof DraggableGroup) {
                            DraggableGroup innerGroup = (DraggableGroup) group.getChildren().get(k);
                            if (innerGroup.getType().equals("Station"))
                                stations.add(innerGroup);
                        }
                    }
                }

            }
        }
        return stations;
    }

    public void removeGridGroup() {
        removeNode(((metWorkspace) app.getWorkspaceComponent()).getGridGroup());
    }

    public void createGridGroup() {
        ((metWorkspace) app.getWorkspaceComponent()).createGrid();
    }

    public boolean isGridVisible() {
        Group grid = ((metWorkspace) app.getWorkspaceComponent()).getGridGroup();
        if (grid.isVisible())
            return true;
        return false;
    }

    public Group getGridGroup() {
        return ((metWorkspace) app.getWorkspaceComponent()).getGridGroup();
    }

    public void addNodeAtIndex(int nodeIndex, Node node) {
        ((metWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(nodeIndex, node);
    }

    public void moveLabel(String selection) {
        DraggableGroup stationGroup = null;
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                if (group.getChildren().get(0) instanceof DraggableStation &&
                        ((DraggableText) group.getChildren().get(1)).getText().equals(selection)) {
                    //This means we found it
                    stationGroup = group;
                } else if (group.getChildren().get(0) instanceof DraggableText) {
                    for (int k = 0; k < group.getChildren().size(); k++) {
                        if (group.getChildren().get(k) instanceof DraggableGroup &&
                                ((DraggableGroup) group.getChildren().get(k)).getChildren().get(0) instanceof DraggableStation) {
                            if (((DraggableText) ((DraggableGroup) group.getChildren().get(k))
                                    .getChildren().get(1)).getText().equals(selection)) {
                                //This means we found it
                                stationGroup = (DraggableGroup) group.getChildren().get(k);
                            }
                        }
                    }
                }
            }
        }
        if (stationGroup != null) {
            DraggableStation station = (DraggableStation) stationGroup.getChildren().get(0);
            DraggableText text = (DraggableText) stationGroup.getChildren().get(1);
            MoveLabel_Transaction mlt = new MoveLabel_Transaction(station, text);
            app.getTPS().addTransaction(mlt);
        }
    }

    public DraggableGroup getLineFromString(String selection) {
        for (int i = 0; i < mapNodes.size(); i++) {
            if (mapNodes.get(i) instanceof DraggableGroup && ((DraggableGroup) mapNodes.get(i)).getType().equals("Line")) {
                DraggableGroup group = (DraggableGroup) mapNodes.get(i);
                if (((DraggableText) group.getChildren().get(0)).getText().equals(selection)) {
                    return group;

                }
            }
        }
        return null;
    }

    public String getBackgroundImagePath() {
        return backgroundImagePath;
    }
    public void setBackgroundImagePath(String backgroundImagePath){
        this.backgroundImagePath = backgroundImagePath;
    }
}


