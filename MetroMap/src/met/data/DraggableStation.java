package met.data;

import javafx.scene.shape.Circle;

/**
 * Created by Kitcatski on 11/18/2017.
 */
public class DraggableStation extends Circle implements Draggable {

    public DraggableStation(int x, int y) {
        setCenterX(x);
        setCenterY(y);
        setRadius(10);
    }

    @Override
    public Draggable makeClone() {
        return null;
    }

    @Override
    public metState getStartingState() {
        return null;
    }

    @Override
    public void start(int x, int y) {

    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - getCenterX();
        double diffY = y - getCenterY();
        double newX = getX() + diffX;
        double newY = getY() + diffY;
        centerXProperty().set(newX);
        centerYProperty().set(newY);
        setCenterX(x);
        setCenterY(y);
    }

    @Override
    public void drag(int x) {

    }

    @Override
    public void size(int x, int y) {

    }

    @Override
    public double getX() {
        return 0;
    }

    @Override
    public double getY() {
        return 0;
    }

    @Override
    public double getWidth() {
        return getLayoutBounds().getWidth();
    }

    @Override
    public double getHeight() {
        return getLayoutBounds().getHeight();
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {

    }

    @Override
    public String getNodeType() {
        return "STATION";
    }

    @Override
    public void setStart(int initX, int initY) {

    }
}
