package met.data;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.text.Text;

import java.util.Collection;

/**
 * Created by Kitcatski on 11/18/2017.
 */
public class DraggableGroup extends Group implements Draggable{
    String type;
    String lineName; // Only if type line
    DraggableGroup connection; //Only if type station
    double connectionX;
    double connectionY;
    int size;


    public DraggableGroup(String type) {
        this.type = type;
    }
    public DraggableGroup(int size){
        this.size = size;
        type = "Station";
    }

    @Override
    public Draggable makeClone() {
        return null;
    }

    @Override
    public metState getStartingState() {
        return null;
    }

    @Override
    public void start(int x, int y) {

    }

    @Override
    public void drag(int x, int y) {

    }

    @Override
    public void drag(int x) {

    }

    @Override
    public void size(int x, int y) {

    }

    @Override
    public double getX() {
        return 0;
    }

    @Override
    public double getY() {
        return 0;
    }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public double getHeight() {
        return 0;
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {

    }

    @Override
    public String getNodeType() {
        return null;
    }

    @Override
    public void setStart(int initX, int initY) {

    }
    public String getType(){
        return type;
    }
    public void setType(String type){this.type = type;}
    public String getLineName(){
        return lineName;

    }
    public void setConnection(DraggableGroup connection, double connectionX, double connectionY){
        this.connection = connection;
        this.connectionX = connectionX;
        this.connectionY = connectionY;
    }
    public void setLineName(String lineName){
        this.lineName = lineName;
    }
    public int getSize(){
        return size;
    }

    public DraggableGroup getConnection() {
        return connection;
    }
    public double getConnectionX(){
        return connectionX;
    }
    public double getConnectionY(){
        return connectionY;
    }
}
