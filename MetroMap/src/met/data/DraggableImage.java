package met.data;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import properties_manager.PropertiesManager;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static djf.language.AppLanguageSettings.FILE_PROTOCOL;
import static met.metPropertyType.DEFAULT_NODE_X;
import static met.metPropertyType.DEFAULT_NODE_Y;

/**
 * Created by Eric on 12/2/2017.
 */
public class DraggableImage extends ImageView implements Draggable {
    double startX;
    double startY;
    String path;
    Image image;

    public DraggableImage(Image image, String url){
        this.image = image;
        setImage(image);
        setX(0.0);
        setY(0.0);
        setOpacity(1.0);
        startX = 0.0;
        startY = 0.0;
        path = url;

    }
    public DraggableImage(String url) {
        setX(0.0);
        setY(0.0);
        setOpacity(1.0);
        startX = 0.0;
        startY = 0.0;
        path = url;
    }

    @Override
    public DraggableImage makeClone() {
//        DraggableImage cloneImage = new DraggableImage();
//        cloneImage.setImage(getImage());
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        cloneImage.setX(Double.parseDouble(props.getProperty(DEFAULT_NODE_X)));
//        cloneImage.setY(Double.parseDouble(props.getProperty(DEFAULT_NODE_Y)));
//        cloneImage.setOpacity(getOpacity());
//        return cloneImage;
        return null;
    }

    @Override
    public metState getStartingState() {
        return metState.STARTING_IMAGE;
    }

    @Override
    public void start(int x, int y) {
        startX = x;
        startY = y;
        setX(x);
        setY(y);
    }

    @Override
    public void setStart(int initStartX, int initStartY) {
        startX = initStartX;
        startY = initStartY;
    }

    @Override
    public void drag(int x, int y) {
        //double diffX = x - (getX() + (getWidth()/2));
        //double diffY = y - (getY() + (getHeight()/2));
        double diffX = x - startX;
        double diffY = y - startY;
        double newX = getX() + diffX;
        double newY = getY() + diffY;
        xProperty().set(newX);
        yProperty().set(newY);
        startX = x;
        startY = y;
    }

    @Override
    public void drag(int x) {

    }

    public String cT(double x, double y) {
        return "(x,y): (" + x + "," + y + ")";
    }

    @Override
    public void size(int x, int y) {
        // WE DON'T RESIZE THE IMAGE
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xProperty().set(initX);
        yProperty().set(initY);
        // WE DON'T RESIZE THE IMAGE
    }

    @Override
    public String getNodeType() {
        return "IMAGE";
    }

    @Override
    public double getWidth() {
        return super.getImage().getWidth();
    }

    @Override
    public double getHeight() {
        return super.getImage().getHeight();
    }

    public String getPath(){
        return path;
    }

    public void setPath(String path) throws IOException {
        this.path = path;

    }
}
