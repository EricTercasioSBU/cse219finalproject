package met.data;

import javafx.scene.text.Text;

import java.util.ArrayList;

/**
 * Created by Kitcatski on 12/8/2017.
 */
public class Route {
    private ArrayList<DraggableGroup> stations;
    private int length;

    public Route() {
        stations = new ArrayList<>();
        length = 0;
    }

    public ArrayList<DraggableGroup> getStations() {
        return stations;
    }
    public void displayStations(){
        System.out.println("STATIONS ||");
        for (int i = 0; i < stations.size(); i++){
            Text text = (Text) stations.get(i).getChildren().get(1);
            System.out.println(i + " " +text.getText());

        }
    }

    public void setStations(ArrayList<DraggableGroup> stations) {
        this.stations = stations;
    }
    public void addStation(DraggableGroup station){
        stations.add(station);
    }
    public void addToLength(int amount){
        length = length + amount;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public DraggableGroup lastStation() {
        return stations.get(stations.size() - 1);
    }
}
