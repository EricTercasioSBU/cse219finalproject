package met.data;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.QuadCurve;

/**
 * This class represents a draggable line for the map
 *
 * @author Eric Tercasio
 * @version 1.0
 */
public class DraggableLine extends Line implements Draggable{
    public DraggableLine(double startX, double startY, double endX, double endY) {
        super(startX, startY, endX, endY);
    }

//      For quad curve, maybe implement later
//    public DraggableLine(double startX, double startY, double controlX, double controlY, double endX, double endY) {
//        super(startX, startY, controlX, controlY, endX, endY);
//        setStrokeWidth(1);
//        setFill(null);
//        setStroke(Color.BLACK);
//    }

    @Override
    public Draggable makeClone() {
        return null;
    }

    @Override
    public metState getStartingState() {
        return null;
    }

    @Override
    public void start(int x, int y) {


    }
    @Override
    public void drag(int x){

    }


    @Override
    public void drag(int x, int y) {

    }

    @Override
    public void size(int x, int y) {

    }

    @Override
    public double getX() {
        return 0;
    }

    @Override
    public double getY() {
        return 0;
    }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public double getHeight() {
        return 0;
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {

    }

    @Override
    public String getNodeType() {
        return "LINE";
    }

    @Override
    public void setStart(int initX, int initY) {

    }
}
