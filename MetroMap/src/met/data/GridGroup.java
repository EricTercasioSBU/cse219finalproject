package met.data;

import javafx.scene.Group;
import javafx.scene.Node;

import java.util.Collection;

/**
 * Created by Amanda on 12/6/2017.
 */
public class GridGroup extends Group {
    public GridGroup() {
    }

    public GridGroup(Node... children) {
        super(children);
    }

    public GridGroup(Collection<Node> children) {
        super(children);
    }
}
