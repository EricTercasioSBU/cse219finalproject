package met;

import djf.AppTemplate;
import met.data.metData;
import met.file.metFiles;
import met.gui.metWelcome;
import met.gui.metWorkspace;


import java.util.Locale;

/**
 * Created by Kitcatski on 11/3/2017.
 */
public class metApp extends AppTemplate {

    /**
     * This hook method must initialize all three components in the proper order ensuring proper dependencies are
     * respected, meaning all proper objects are already constructed when they are needed for use, since some may need
     * others for initialization.
     */
    @Override
    public void buildAppComponentsHook() {
        // CONSTRUCT ALL THREE COMPONENTS. NOTE THAT FOR THIS APP
        // THE WORKSPACE NEEDS THE DATA COMPONENT TO EXIST ALREADY
        // WHEN IT IS CONSTRUCTED, AND THE DATA COMPONENT NEEDS THE
        // FILE COMPONENT SO WE MUST BE CAREFUL OF THE ORDER
        fileComponent = new metFiles();
        dataComponent = new metData(this);
        workspaceComponent = new metWorkspace(this);
        appWelcomeComponent = new metWelcome();

    }
    /**
     * This is where program execution begins. Since this is a JavaFX app it will simply call launch, which gets JavaFX
     * rolling, resulting in sending the properly initialized Stage (i.e. window) to the start method inherited from
     * AppTemplate, defined in the Desktop Java Framework.
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }

}
