package met.file;

import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import met.data.*;

import javax.imageio.ImageIO;
import javax.json.*;
import javax.json.stream.JsonGenerator;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kitcatski on 11/3/2017.
 */
public class metFiles implements AppFileComponent {
    // FOR JSON LOADING
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_SHAPES = "shapes";
    static final String JSON_SHAPE = "shape";
    static final String JSON_COLOR = "color";
    static final String JSON_LINES = "lines";
    static final String JSON_CIRCULAR = "circular";
    static final String JSON_STATIONS = "stations";
    static final String JSON_STATION_NAMES = "station_names";
    static final String JSON_NAME = "name";
    static final String JSON_TYPE = "type";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_START_X = "start_x";
    static final String JSON_START_Y = "start_y";
    static final String JSON_END_X = "end_x";
    static final String JSON_END_Y = "end_y";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_FILL_COLOR = "fill_color";
    static final String JSON_OUTLINE_COLOR = "outline_color";
    static final String JSON_OUTLINE_THICKNESS = "outline_thickness";
    static final String JSON_TEXT = "text";
    //For Group
    static final String JSON_SIZE = "size";
    static final String JSON_GROUP_TYPE = "group_type";

    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        metData dataManager = (metData)data;
        boolean visible = false;
        // FIRST THE BACKGROUND COLOR
        JsonObject bgColorJson;
        if(dataManager.getBackgroundImagePath() != null){
            String path = dataManager.getBackgroundImagePath();
            bgColorJson = makeJsonColorObject(null,path);
        }else {
            if(dataManager.getBackgroundColor() != null);
            {
                Color bgColor = dataManager.getBackgroundColor();
                bgColorJson = makeJsonColorObject(bgColor, null);
            }
        }
        // NOW BUILD ALL OF THE JSON OBJECTS TO SAVE
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        //Everything on the pane is a group or a shape
        ObservableList<Node> nodes = dataManager.getMapNodes();
        if(dataManager.getGridGroup() != null) {
            if (dataManager.isGridVisible()) {
                visible = true;
            }
            ((metData) data).removeGridGroup();
        }

        for (Node node : nodes) {
            if(node instanceof DraggableGroup){//IF ITS A GROUP (MOST ARE)
                DraggableGroup group = (DraggableGroup)node;
                makeJsonGroupObject(group,arrayBuilder);
                for(int i = 0; i < group.getChildren().size(); i++){
                    //Check if theres a group within the group (this could only happen once)
                    if(group.getChildren().get(i) instanceof DraggableGroup){
                        DraggableGroup innerGroup = (DraggableGroup)group.getChildren().get(i);
                        makeJsonGroupObject(innerGroup,arrayBuilder);
                        //Go within that group to get its shapes
                        for(int k = 0; k < ((DraggableGroup) group.getChildren().get(i)).getChildren().size(); k++){
                            Shape shape = (Shape) ((DraggableGroup) group.getChildren().get(i)).getChildren().get(k);
                            buildShapeObject(shape,arrayBuilder);
                        }
                    }else {
                        Shape shape = (Shape) group.getChildren().get(i);
                        buildShapeObject(shape, arrayBuilder);
                    }
                }

            }else {//IF ITS A STANDALONE SHAPE(TEXT, IMAGE ECT)
                if(node instanceof DraggableImage){
                    buildJsonImageObject((DraggableImage) node,arrayBuilder);
                }else
                buildShapeObject(node,arrayBuilder);
            }
        }
        JsonArray shapesArray = arrayBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_SHAPES,shapesArray)
                .build();
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        dataManager.createGridGroup();
        if(visible){
            dataManager.getGridGroup().setVisible(true);
        }
    }


    private JsonObject makeJsonColorObject(Color color, String path) {
        JsonObject colorJson = null;
        if(color != null) {
            colorJson = Json.createObjectBuilder()
                    .add(JSON_RED, color.getRed())
                    .add(JSON_GREEN, color.getGreen())
                    .add(JSON_BLUE, color.getBlue())
                    .add(JSON_ALPHA, color.getOpacity())
                    .add("image_path", "").build();
        }else if(path != null){
            colorJson = Json.createObjectBuilder()
                    .add(JSON_RED, 0)
                    .add(JSON_GREEN, 0)
                    .add(JSON_BLUE, 0)
                    .add(JSON_ALPHA, 0)
                    .add("image_path", path).build();
        }
        return colorJson;
    }
    private void makeJsonGroupObject(DraggableGroup group, JsonArrayBuilder arrayBuilder){
        JsonObject groupJson = Json.createObjectBuilder()
                .add(JSON_TYPE,"GROUP")
                .add(JSON_GROUP_TYPE,group.getType())
                .add(JSON_SIZE, group.getChildren().size()).build();
        arrayBuilder.add(groupJson);
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT

        metData dataManager = (metData)data;
        dataManager.resetData();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // LOAD THE BACKGROUND COLOR
        String url = getBackgroundUrl(json, JSON_BG_COLOR);
        Color bgColor = loadColor(json, JSON_BG_COLOR);
        if(!url.isEmpty()){
            Image image = new Image(url);
            BackgroundSize backgroundSize = new BackgroundSize(dataManager.getCanvas().getWidth(),dataManager.getCanvas().getHeight(), true, true, true, false);
            BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
            dataManager.setBackgroundImage(backgroundImage);
            dataManager.setBackgroundImagePath(url);
        }else {
            dataManager.setBackgroundColor(bgColor);
        }
        //  LOAD THE SHAPES
        JsonArray jsonShapeArray = json.getJsonArray(JSON_SHAPES);
        Boolean addingToGroup = false;
        Boolean addingToInnerGroup = false;
        int amountToAdd = 0;
        DraggableGroup currentGroup = null;
        DraggableGroup currentInnerGroup = null;
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            Node shape = loadShape(jsonShape);
            if(addingToInnerGroup){
                DraggableStation station = (DraggableStation) shape;
                JsonObject nextJsonShape = jsonShapeArray.getJsonObject(i + 1);
                DraggableText text = (DraggableText) loadShape(nextJsonShape);
                currentInnerGroup.getChildren().addAll(station,text);
                i++;
                addingToInnerGroup = false;
            }else if(addingToGroup){
                if(amountToAdd != 0) {
                    if (shape instanceof DraggableGroup) {
                        addingToInnerGroup = true;
                        currentInnerGroup = (DraggableGroup) shape;
                    }
                    currentGroup.getChildren().add(shape);
                    amountToAdd--;
                }else{
                    addingToGroup = false;
                    i--;
                }
            }else if(shape instanceof DraggableGroup){
                    addingToGroup = true;
                    int size = ((DraggableGroup) shape).getSize();
                    amountToAdd = size;
                    dataManager.addNode(shape);
                    currentGroup = (DraggableGroup) shape;


            } else {
                dataManager.addNode(shape);
                if(shape instanceof DraggableImage){
                    System.out.println(dataManager.getMapNodes());
                }
            }
        }
        dataManager.bindAllNodes();
        dataManager.loadIntoComboBoxes();


}

    private String getBackgroundUrl(JsonObject json, String jsonBgColor) {
        JsonObject jsonColor = json.getJsonObject(jsonBgColor);
        String url = getDataAsString(jsonColor, "image_path");
        return url;
    }

    private Node loadShape(JsonObject jsonShape) throws IOException {
        // FIRST BUILD THE PROPER SHAPE TYPE

        String type = jsonShape.getString(JSON_TYPE);
        Node shape = null;
        if(type.equals("GROUP")){
            double children = getDataAsDouble(jsonShape,"size");
            String groupType = getDataAsString(jsonShape,JSON_GROUP_TYPE);
            shape = new DraggableGroup((int)children);
            ((DraggableGroup)shape).setType(groupType);
            return shape;
        }
        if (type.equals("LINE")) {
            shape = new DraggableLine(0,0,0,0);
            Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            ((DraggableLine)shape).setStroke(outlineColor);
            double startX = getDataAsDouble(jsonShape, JSON_START_X);
            double startY = getDataAsDouble(jsonShape, JSON_START_Y);
            double endX = getDataAsDouble(jsonShape, JSON_END_X);
            double endY = getDataAsDouble(jsonShape, JSON_END_Y);
            ((DraggableLine)shape).setStartX(startX);
            ((DraggableLine)shape).setStartY(startY);
            ((DraggableLine)shape).setEndX(endX);
            ((DraggableLine)shape).setEndY(endY);

        }
        if(type.equals("STATION")) {
            shape = new DraggableStation(0,0);
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            ((DraggableStation)shape).setFill(fillColor);
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            ((DraggableStation)shape).setCenterX(x);
            ((DraggableStation)shape).setCenterY(y);
        }
        if(type.equals("TEXT")){

            shape = new DraggableText("");
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            ((DraggableText)shape).setFill(fillColor);
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            ((DraggableText)shape).setX(x);
            ((DraggableText)shape).setY(y);
            String text = getDataAsString(jsonShape, JSON_TEXT);
            ((DraggableText) shape).setText(text);
        }
        if(type.equals("IMAGE")){
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            String imageUrl = getDataAsString(jsonShape, "image_URL");
            double width = getDataAsDouble(jsonShape, JSON_WIDTH);
            double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
            DraggableImage image = new DraggableImage(new Image(imageUrl), imageUrl);
            image.setX(x);
            image.setY(y);
            image.setFitWidth(width);
            image.setFitHeight(height);

            return image;

        }
        //Text and station have no outline
        //Line has no fill
        //Start with what they all have
        //DraggableProperties

//        double width = getDataAsDouble(jsonShape, JSON_WIDTH);
//        double height = getDataAsDouble(jsonShape, JSON_HEIGHT);

        // THEN LOAD ITS FILL AND OUTLINE PROPERTIES
        double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
        ((Shape)shape).setStrokeWidth(outlineThickness);


        // ALL DONE, RETURN IT
        return shape;
    }
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        metData dataManager = (metData)data;
        String name = filePath.substring(16,filePath.length() - 11);
        //FIRST THE NAME
        // BUILD ALL OF THE JSON OBJECTS TO SAVE
        JsonArrayBuilder linesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder stationsArrayBuilder = Json.createArrayBuilder();
        //Everything on the pane is a group or a shape
        ObservableList<Node> nodes = dataManager.getMapNodes();
        for (Node node : nodes) {
            if(node instanceof DraggableGroup && ((DraggableGroup) node).getType().equals("Line")){//IF ITS A LINE GROUP (MOST ARE)
                DraggableGroup group = (DraggableGroup)node;
                ArrayList<DraggableGroup> stationsInLine =  new ArrayList<>();
                for(int i = 0; i < group.getChildren().size(); i++) {
                    //Check if there's a group within the group (This means the line has stations)
                    if (group.getChildren().get(i) instanceof DraggableGroup) {
                        DraggableGroup innerGroup = (DraggableGroup) group.getChildren().get(i);
                        buildExportStationsObject(innerGroup,stationsArrayBuilder);
                        stationsInLine.add(innerGroup);

                    }
                }
                buildExportLineObject(group, stationsInLine, linesArrayBuilder);
            }else if(node instanceof DraggableGroup && ((DraggableGroup) node).getType().equals("Station")) {//IF ITS A STANDALONE SHAPE(TEXT, IMAGE ECT)
                DraggableGroup group = (DraggableGroup)node;
                buildExportStationsObject(group,stationsArrayBuilder);
            }
        }

        JsonArray linesArray = linesArrayBuilder.build();
        JsonArray stationsArray = stationsArrayBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_NAME, name)
                .add(JSON_LINES,linesArray)
                .add(JSON_STATIONS, stationsArray)
                .build();
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

         //INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

        Pane canvas = dataManager.getCanvas();
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        File file = new File(filePath.substring(0,filePath.length() - 5) + ".png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }


    }

    private void buildExportStationsObject(DraggableGroup group, JsonArrayBuilder stationsArrayBuilder) {
        DraggableStation station = (DraggableStation)group.getChildren().get(0);
        DraggableText stationText = (DraggableText)group.getChildren().get(1);
        String name = stationText.getText();
        double x = station.getCenterX();
        double y = station.getCenterY();
        JsonObject stationJson = Json.createObjectBuilder()
                .add(JSON_NAME, name)
                .add(JSON_X,x )
                .add(JSON_Y,y).build();
        stationsArrayBuilder.add(stationJson);
    }

    private void buildExportLineObject(DraggableGroup group, ArrayList<DraggableGroup> stationsInLine, JsonArrayBuilder arrayBuilder) {
        DraggableText lineText = (DraggableText) group.getChildren().get(0);
        DraggableLine line = (DraggableLine)group.getChildren().get(1);
        JsonObject outlineColorJson = makeJsonColorObject((Color) line.getStroke(),null);
        String lineName = lineText.getText();
        JsonArrayBuilder stationsArrayBuilder = Json.createArrayBuilder();
        for(int i = 0; i < stationsInLine.size(); i++){
            String stationName = ((DraggableText)stationsInLine.get(i).getChildren().get(1)).getText();
            stationsArrayBuilder.add(stationName);
        }
        JsonObject shapeJson = Json.createObjectBuilder()
                .add(JSON_NAME, lineName)
                .add(JSON_CIRCULAR, false)
                .add(JSON_COLOR, outlineColorJson)
                .add(JSON_STATION_NAMES,stationsArrayBuilder).build();
        arrayBuilder.add(shapeJson);

    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {

    }
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    private Color loadColor(JsonObject json, String colorToGet) {
        JsonObject jsonColor = json.getJsonObject(colorToGet);
        double red = getDataAsDouble(jsonColor, JSON_RED);
        double green = getDataAsDouble(jsonColor, JSON_GREEN);
        double blue = getDataAsDouble(jsonColor, JSON_BLUE);
        double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
        Color loadedColor = new Color(red, green, blue, alpha);
        return loadedColor;
    }

    private double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigDecimalValue().doubleValue();
    }
    private String getDataAsString(JsonObject json, String dataName){
        JsonValue value = json.get(dataName);
        JsonString a = (JsonString)value;
        return a.getString();
    }
    public void buildShapeObject(Node node, JsonArrayBuilder arrayBuilder){
        Shape shape = (Shape) node;
        Draggable draggableShape = ((Draggable) shape);
        String type = draggableShape.getNodeType();
        double x = draggableShape.getX();
        double y = draggableShape.getY();
        double width = draggableShape.getWidth();
        double height = draggableShape.getHeight();
        double outlineThickness = shape.getStrokeWidth();

        //Line has no fill
        if(type.equals("LINE")){
            JsonObject outlineColorJson = makeJsonColorObject((Color) shape.getStroke(),null);
            double startX = ((DraggableLine)node).getStartX();
            double startY = ((DraggableLine)node).getStartY();
            double endX = ((DraggableLine)node).getEndX();
            double endY = ((DraggableLine)node).getEndY();
            JsonObject shapeJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, type)
                    .add(JSON_START_X, startX)
                    .add(JSON_START_Y, startY)
                    .add(JSON_END_X, endX)
                    .add(JSON_END_Y, endY)
                    .add(JSON_WIDTH, width)
                    .add(JSON_HEIGHT, height)
                    .add(JSON_OUTLINE_COLOR,outlineColorJson)
                    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
            arrayBuilder.add(shapeJson);
        }
        //Text has no outline
        if(type.equals("TEXT")){
            JsonObject fillColorJson = makeJsonColorObject((Color) shape.getFill(),null);
            String text = ((DraggableText)shape).getText();
            JsonObject shapeJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, type)
                    .add(JSON_TEXT, text)
                    .add(JSON_X, x)
                    .add(JSON_Y, y)
                    .add(JSON_WIDTH, width)
                    .add(JSON_HEIGHT, height)
                    .add(JSON_FILL_COLOR,fillColorJson)
                    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
            arrayBuilder.add(shapeJson);
        }
        //Station has no outline
        if(type.equals("STATION")){
            JsonObject fillColorJson = makeJsonColorObject((Color) shape.getFill(),null);
            x = ((DraggableStation)shape).getCenterX();
            y = ((DraggableStation)shape).getCenterY();
            JsonObject shapeJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, type)
                    .add(JSON_X, x)
                    .add(JSON_Y, y)
                    .add(JSON_WIDTH, width)
                    .add(JSON_HEIGHT, height)
                    .add(JSON_FILL_COLOR,fillColorJson)
                    .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
            arrayBuilder.add(shapeJson);
        }
    }
    public void buildJsonImageObject(DraggableImage image, JsonArrayBuilder arrayBuilder){
        String type = image.getNodeType();
        double x = image.getX();
        double y = image.getY();
        double width = image.getWidth();
        double height = image.getHeight();
        String imageURL = ((DraggableImage)image).getPath();


        JsonObject shapeJson = Json.createObjectBuilder()
                .add(JSON_TYPE, type)
                .add(JSON_X, x)
                .add(JSON_Y, y)
                .add(JSON_WIDTH, width)
                .add(JSON_HEIGHT, height)
                .add("image_URL",imageURL).build();
        arrayBuilder.add(shapeJson);
    }



}
