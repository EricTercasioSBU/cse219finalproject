package met.css;

/**
 * Created by Kitcatski on 11/3/2017.
 */
public class metStyle {
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS metWorkspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    public static final String CLASS_MAX_PANE = "max_pane";
    public static final String CLASS_RENDER_CANVAS = "render_canvas";
    public static final String CLASS_BUTTON = "button";
    public static final String CLASS_EDIT_TOOLBAR = "edit_toolbar";
    public static final String CLASS_EDIT_TOOLBAR_ROW = "edit_toolbar_row";
    public static final String CLASS_COLOR_CHOOSER_PANE = "color_chooser_pane";
    public static final String CLASS_COLOR_CHOOSER_CONTROL = "color_chooser_control";
    public static final String EMPTY_TEXT = "";
    public static final String TWO_LINE_BUTTON = "two_line_buttons";
    public static final String INNER_TOOLBAR_ROW = "inner_toolbox_row";
    public static final String RECENT_WORK_SIDE_ROW = "recent_work_side_row";
    public static final String BLUE_BUTTONS = "blue_buttons";
    public static final int BUTTON_TAG_WIDTH = 75;
}
